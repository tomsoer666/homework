package lesson22.services.impl;

import lesson22.models.Passport;
import lesson22.repository.PassportRepository;
import lesson22.services.SpringDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Qualifier("passportService")
public class PassportSpringService implements SpringDaoService<Passport> {
    private final PassportRepository repository;

    @Autowired
    public PassportSpringService(PassportRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Passport entity) {
        repository.save(entity);
    }

    @Override
    public void update(Passport entity) {
        repository.save(entity);
    }

    @Override
    public Passport findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void delete(Passport entity) {
        repository.delete(entity);
    }

    @Override
    public Set<Passport> findAll() {
        Set<Passport> passports = new HashSet<>();
        repository.findAll().forEach(passports::add);
        return passports;
    }

    @Override
    public Object findByPassport(Passport passport) {
        return null;
    }
}
