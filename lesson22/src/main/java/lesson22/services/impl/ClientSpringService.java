package lesson22.services.impl;

import lesson22.models.Client;
import lesson22.models.Passport;
import lesson22.repository.ClientRepository;
import lesson22.services.SpringDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Qualifier("clientService")
public class ClientSpringService implements SpringDaoService<Client> {
    private final ClientRepository repository;

    @Autowired
    public ClientSpringService(ClientRepository clientRepository) {
        this.repository = clientRepository;
    }

    @Override
    public void save(Client entity) {
        repository.save(entity);
    }

    @Override
    public void update(Client entity) {
        repository.save(entity);
    }

    @Override
    public Client findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void delete(Client entity) {
        repository.delete(entity);
    }

    @Override
    public Set<Client> findAll() {
        Set<Client> clients = new HashSet<>();
        repository.findAll().forEach(clients::add);
        return clients;
    }

    @Override
    public Object findByPassport(Passport passport) {
        return repository.findByPassport(passport);
    }
}
