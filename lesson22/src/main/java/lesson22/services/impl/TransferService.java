package lesson22.services.impl;

import lesson22.dao.Dao;
import lesson22.dao.impl.TransferDao;
import lesson22.models.Transfer;
import lesson22.services.DaoService;

import java.util.Set;

public class TransferService implements DaoService<Transfer> {
    private final Dao<Transfer> transferDao = new TransferDao();

    @Override
    public void save(Transfer entity) {
        transferDao.save(entity);
    }

    @Override
    public void update(Transfer entity) {
        transferDao.update(entity);
    }

    @Override
    public Transfer findById(Long id) {
        return transferDao.findById(id);
    }

    @Override
    public void delete(Transfer entity) {
        transferDao.delete(entity);
    }

    @Override
    public Set<Transfer> findAll() {
        return transferDao.findAll();
    }
}
