package lesson22.services.impl;

import lesson22.dao.Dao;
import lesson22.dao.impl.ClientDao;
import lesson22.models.Client;
import lesson22.services.DaoService;

import java.util.Set;

public class ClientService implements DaoService<Client> {
    private final Dao<Client> clientDao = new ClientDao();

    @Override
    public void save(Client entity) {
        clientDao.save(entity);
    }

    @Override
    public void update(Client entity) {
        clientDao.update(entity);
    }

    @Override
    public Client findById(Long id) {
        return clientDao.findById(id);
    }

    @Override
    public void delete(Client entity) {
        clientDao.delete(entity);
    }

    @Override
    public Set<Client> findAll() {
        return clientDao.findAll();
    }
}
