package lesson22.services.impl;

import lesson22.dao.Dao;
import lesson22.dao.impl.PassportDao;
import lesson22.models.Passport;
import lesson22.services.DaoService;

import java.util.Set;

public class PassportService implements DaoService<Passport> {
    private final Dao<Passport> passportDao = new PassportDao();

    @Override
    public void save(Passport entity) {
        passportDao.save(entity);
    }

    @Override
    public void update(Passport entity) {
        passportDao.update(entity);
    }

    @Override
    public Passport findById(Long id) {
        return passportDao.findById(id);
    }

    @Override
    public void delete(Passport entity) {
        passportDao.delete(entity);
    }

    @Override
    public Set<Passport> findAll() {
        return passportDao.findAll();
    }
}
