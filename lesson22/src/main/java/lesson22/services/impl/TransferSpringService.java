package lesson22.services.impl;

import lesson22.models.Passport;
import lesson22.models.Transfer;
import lesson22.repository.TransferRepository;
import lesson22.services.SpringDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Qualifier("transferService")
public class TransferSpringService implements SpringDaoService<Transfer> {
    private final TransferRepository repository;

    @Autowired
    public TransferSpringService(TransferRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Transfer entity) {
        repository.save(entity);
    }

    @Override
    public void update(Transfer entity) {
        repository.save(entity);
    }

    @Override
    public Transfer findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void delete(Transfer entity) {
        repository.delete(entity);
    }

    @Override
    public Set<Transfer> findAll() {
        Set<Transfer> transfers = new HashSet<>();
        repository.findAll().forEach(transfers::add);
        return transfers;
    }

    @Override
    public Object findByPassport(Passport passport) {
        return repository.findByPassport(passport.getId());
    }
}
