package lesson22.services;

import java.util.Set;

public interface DaoService<T> {
    public void save(T entity);

    public void update(T entity);

    public T findById(Long id);

    public void delete(T entity);

    public Set<T> findAll();
}
