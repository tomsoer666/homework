package lesson22.services;

import lesson22.models.Passport;

import java.util.Set;

public interface SpringDaoService<T> {
    public void save(T entity);

    public void update(T entity);

    public T findById(Long id);

    public void delete(T entity);

    public Set<T> findAll();

    public Object findByPassport(Passport passport);
}
