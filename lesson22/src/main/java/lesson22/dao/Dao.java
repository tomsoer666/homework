package lesson22.dao;

import java.util.Set;

public interface Dao<T> {
    public void save(T entity);

    public void update(T entity);

    public T findById(Long id);

    public void delete(T entity);

    public Set<T> findAll();
}
