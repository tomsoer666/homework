package lesson22;

import lesson22.models.Client;
import lesson22.models.Passport;
import lesson22.models.Transfer;
import lesson22.services.SpringDaoService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class SpringApplication {
    private final static Logger LOGGER = Logger.getLogger(SpringApplication.class);

    public static void main(String[] args) {
        LOGGER.info("Initiate spring context");
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext();
        context.scan("lesson22");
        context.refresh();
        SpringDaoService<Client> clientService =
                BeanFactoryAnnotationUtils
                        .qualifiedBeanOfType(context.getBeanFactory(),
                                SpringDaoService.class, "clientService");
        SpringDaoService<Transfer> transferService =
                BeanFactoryAnnotationUtils
                        .qualifiedBeanOfType(context.getBeanFactory(),
                                SpringDaoService.class, "transferService");
        SpringDaoService<Passport> passportService =
                BeanFactoryAnnotationUtils
                        .qualifiedBeanOfType(context.getBeanFactory(),
                                SpringDaoService.class, "passportService");
        LOGGER.info("Initialize starting data");
        initData(clientService);
        Passport passport = passportService.findById(2L);
        LOGGER.info("Get passport by id 2: " + passport);
        Client client = (Client) clientService.findByPassport(passport);
        LOGGER.info("Get client by passport: " + client);
        LOGGER.info("Get all client transfers by passport: " + transferService.findByPassport(passport));
    }

    private static void initData(SpringDaoService<Client> springDaoService) {
        Set<Transfer> transfers = new HashSet<>();
        transfers.add(new Transfer(1000L, new Date(94, 11, 13)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        Client client = new Client();
        client.setEmail("pushkin992@mail.ru");
        Client client2 = new Client();
        client2.setEmail("bogdanov1234@yandex.ru");
        client.setTransfers(transfers);
        Passport passport = new Passport(
                3256,
                535632,
                "Alex",
                "Pushkin",
                43,
                new Date(76, 2, 3),
                client);
        Passport passport2 = new Passport(
                5356,
                528672,
                "Sergey",
                "Bogdanov",
                53,
                new Date(66, 5, 23),
                client2);
        client.setPassport(passport);
        client2.setPassport(passport2);
        springDaoService.save(client);
        springDaoService.save(client2);
    }
}
