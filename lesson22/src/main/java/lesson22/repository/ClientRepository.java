package lesson22.repository;

import lesson22.models.Client;
import lesson22.models.Passport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
    Client findByPassport(Passport passport);
}
