package lesson22.repository;

import lesson22.models.Passport;
import lesson22.models.Transfer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
    @Query(value =
            "SELECT *\n" +
                    "FROM Transfer tr\n" +
                    "JOIN Client cl\n" +
                    "ON cl.id = tr.client_fk\n" +
                    "JOIN Passport ps\n" +
                    "ON ps.id = cl.passport_fk\n" +
                    "WHERE ps.id = ?1",
            nativeQuery = true)
    Set<Transfer> findByPassport(Long passport);
}
