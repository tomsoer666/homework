package lesson22.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Passport {
    @Id
    @GeneratedValue
    @Expose
    private Long id;
    @Expose
    private Integer series;
    @Expose
    private Integer number;
    @Expose
    private String fullName;
    @Expose
    private String secondName;
    @Expose
    private int age;
    @Expose
    private Date birthDate;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "client_fk")
    private Client client;

    public Passport(Integer series, Integer number, String fullName, String secondName, int age, Date birthDate, Client client) {
        this.series = series;
        this.number = number;
        this.fullName = fullName;
        this.secondName = secondName;
        this.age = age;
        this.birthDate = birthDate;
        this.client = client;
    }

    public Passport() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSeries() {
        return series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        com.google.gson.Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
