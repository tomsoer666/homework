package lesson22.models;

import com.google.gson.GsonBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Transfer {
    @Id
    @GeneratedValue
    private Long id;
    private Long paymentAmount;
    private Date transactionDate;

    public Transfer() {
    }

    public Transfer(Long paymentAmount, Date transactionDate) {
        this.paymentAmount = paymentAmount;
        this.transactionDate = transactionDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Long paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public String toString() {
        com.google.gson.Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
