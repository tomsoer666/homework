package lesson22;

import lesson22.models.Client;
import lesson22.models.Passport;
import lesson22.models.Transfer;
import lesson22.services.DaoService;
import lesson22.services.impl.ClientService;
import lesson22.services.impl.PassportService;
import lesson22.services.impl.TransferService;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

public class HibernateApplication {
    private static final DaoService<Client> CLIENT_SERVICE = new ClientService();
    private static final DaoService<Passport> PASSPORT_SERVICE = new PassportService();
    private static final DaoService<Transfer> TRANSFER_SERVICE = new TransferService();
    private static final Logger LOGGER = Logger.getLogger(HibernateApplication.class);
    private static final String CLIENTS = "All clients: ";

    public static void main(String[] args) {
        initData();
        Client client = new Client();
        client.setEmail("myMail@mail.ru");
        Passport passport = new Passport(
                6435,
                643523,
                "Ilya",
                "Trukhov",
                21,
                new Date(97, 12, 24),
                client);
        client.setPassport(passport);
        LOGGER.info("Create and save " + client);
        CLIENT_SERVICE.save(client);
        Set<Client> clients = CLIENT_SERVICE.findAll();
        client = clients.stream().findFirst().orElse(null);
        LOGGER.info(CLIENTS + clients);
        Client firstClient = CLIENT_SERVICE.findById(client.getId());
        LOGGER.info("Finding first client by his id" + firstClient);
        firstClient.setEmail("newemail@mail.ru");
        LOGGER.info("Updating " + firstClient);
        CLIENT_SERVICE.update(firstClient);
        LOGGER.info(CLIENTS + CLIENT_SERVICE.findAll());
        CLIENT_SERVICE.delete(firstClient);
        LOGGER.info("Delete " + firstClient);
        LOGGER.info(CLIENTS + CLIENT_SERVICE.findAll());
    }

    private static void initData() {
        Set<Transfer> transfers = new HashSet<>();
        transfers.add(new Transfer(1000L, new Date(94, 11, 13)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        transfers.add(new Transfer(5000L, new Date(99, 4, 16)));
        Client client = new Client();
        client.setEmail("pushkin992@mail.ru");
        Client client2 = new Client();
        client2.setEmail("bogdanov1234@yandex.ru");
        client.setTransfers(transfers);
        Passport passport = new Passport(
                3256,
                535632,
                "Alex",
                "Pushkin",
                43,
                new Date(76, 2, 3),
                client);
        Passport passport2 = new Passport(
                5356,
                528672,
                "Sergey",
                "Bogdanov",
                53,
                new Date(66, 5, 23),
                client2);
        client.setPassport(passport);
        client2.setPassport(passport2);
        CLIENT_SERVICE.save(client);
        CLIENT_SERVICE.save(client2);
    }
}
