package lesson5.validation.impl;

import lesson5.annotations.*;
import lesson5.validation.Validator;

import java.lang.reflect.Field;

public class ValidatorImpl<T> implements Validator<T> {
    @Override
    public void validate(T obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            validateNotNullValue(field, obj);
            validateNotEmptyString(field, obj);
            validateStringMinLength(field, obj);
            validateStringMaxLength(field, obj);
            validateIntegerMinValue(field, obj);
            validateIntegerMaxValue(field, obj);
        }
    }

    private void validateStringMinLength(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(MinLength.class)) {
            MinLength annotation = field.getAnnotation(MinLength.class);
            int minLength = annotation.value();
            String value = field.get(obj).toString();
            if (value.length() < minLength) {
                throw new IllegalArgumentException(field.getName() +
                        " length must be more then " + minLength);
            }
        }
    }

    private void validateStringMaxLength(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(MaxLength.class)) {
            MaxLength annotation = field.getAnnotation(MaxLength.class);
            int maxLength = annotation.value();
            String value = field.get(obj).toString();
            if (value.length() > maxLength) {
                throw new IllegalArgumentException(field.getName() +
                        " length must be less then " + maxLength);
            }
        }
    }

    private void validateIntegerMinValue(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(Min.class)) {
            Min annotation = field.getAnnotation(Min.class);
            int min = annotation.value();
            int value = field.getInt(obj);
            if (value < min) {
                throw new IllegalArgumentException(field.getName() +
                        " must be more then " + min);
            }
        }
    }

    private void validateIntegerMaxValue(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(Max.class)) {
            Max annotation = field.getAnnotation(Max.class);
            int max = annotation.value();
            int value = field.getInt(obj);
            if (value > max) {
                throw new IllegalArgumentException(field.getName() +
                        " must be less then " + max);
            }
        }
    }

    private void validateNotEmptyString(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(NotEmpty.class)) {
            String value = field.get(obj).toString().trim();
            if (value.equals("")) {
                throw new IllegalArgumentException(field.getName() +
                        " Value must not be empty");
            }
        }
    }

    private void validateNotNullValue(Field field, T obj) throws IllegalAccessException {
        if (field.isAnnotationPresent(NotNull.class)) {
            if (field.get(obj) == null) {
                throw new IllegalArgumentException(field.getName() +
                        " Value must not be null");
            }
        }
    }
}
