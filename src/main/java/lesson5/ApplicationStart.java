package lesson5;

import lesson5.models.Person;
import lesson5.validation.Validator;
import lesson5.validation.impl.ValidatorImpl;

import java.util.Date;

public class ApplicationStart {
    public static void main(String[] args) {
        Validator<Person> validator = new ValidatorImpl<>();
        Person correctPerson = new Person("Ilya",
                "Trukhov",
                "Igorevich",
                new Date(),
                23,
                "password");
        Person wrongMaxLengthPerson = new Person(
                "IlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlyaIlya",
                "Trukhov",
                "Igorevich",
                new Date(),
                23,
                "password");
        Person wrongMinLengthPerson = new Person("Ilya",
                "Trukhov",
                "Igorevich",
                new Date(),
                23,
                "pass");
        Person wrongMinPerson = new Person("Ilya",
                "Trukhov",
                "Igorevich",
                new Date(),
                -1,
                "password");
        Person wrongMaxPerson = new Person("Ilya",
                "Trukhov",
                "Igorevich",
                new Date(),
                130,
                "password");
        Person wrongEmptyPerson = new Person("",
                "Trukhov",
                "Igorevich",
                new Date(),
                23,
                "password");
        Person wrongNullPerson = new Person("Ilya",
                "Trukhov",
                "Igorevich",
                null,
                23,
                "password");
        try {
            validator.validate(correctPerson);
            System.out.println("Person correct");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
