package lesson11.interfaces.impl;

import lesson11.exceptions.FailedThreadException;
import lesson11.interfaces.ThreadPool;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolImpl implements ThreadPool {
    private final Thread[] threadPool;
    private final Thread callbackThread;
    private final AtomicInteger exceptionCount = new AtomicInteger(0);

    public ThreadPoolImpl(Runnable callback, Runnable... tasks) {
        threadPool = new Thread[tasks.length];
        for (int i = 0; i < threadPool.length; i++) {
            threadPool[i] = new Thread(tasks[i]);
            threadPool[i].setUncaughtExceptionHandler((t, e) -> {
                exceptionCount.incrementAndGet();
                throw new FailedThreadException("Thread cause exception");
            });
        }
        callbackThread = new Thread(callback);
    }

    @Override
    public void execute() {
        for (Thread thread : threadPool) {
            thread.start();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int counterEndedThread = 0;
        while (counterEndedThread != threadPool.length) {
            for (Thread thread : threadPool) {
                if (thread.getState().equals(Thread.State.TERMINATED)) {
                    counterEndedThread++;
                } else {
                    counterEndedThread = 0;
                    break;
                }
            }
        }
        callbackThread.start();
    }

    @Override
    public Thread[] getThreadPool() {
        return threadPool;
    }

    @Override
    public AtomicInteger getExceptionCount() {
        return exceptionCount;
    }
}
