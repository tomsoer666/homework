package lesson11.interfaces.impl;

import lesson11.interfaces.Context;
import lesson11.interfaces.ThreadPool;

public class ContextImpl implements Context {
    private final ThreadPool threadPool;
    private int interruptedTaskCount = 0;

    public ContextImpl(ThreadPool threadPool) {
        this.threadPool = threadPool;
    }


    @Override
    public int getCompletedTaskCount() {
        int count = 0;
        for (Thread thread : threadPool.getThreadPool()) {
            if (thread.getState().equals(Thread.State.TERMINATED)) {
                count++;
            }
        }
        int exceptionCount = threadPool.getExceptionCount().get();
        return count - exceptionCount;
    }

    @Override
    public int getFailedTaskCount() {
        return threadPool.getExceptionCount().get();
    }

    @Override
    public int getInterruptedTaskCount() {
        return interruptedTaskCount;
    }

    @Override
    public void interrupt() {
        for (Thread thread : threadPool.getThreadPool()) {
            if (thread.getState().equals(Thread.State.NEW)) {
                thread.stop();
                interruptedTaskCount++;
            }
        }
    }

    @Override
    public boolean isFinished() {
        for (Thread thread : threadPool.getThreadPool()) {
            if (!thread.getState().equals(Thread.State.TERMINATED)) {
                return false;
            }
        }
        return true;
    }
}
