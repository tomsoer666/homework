package lesson11.interfaces.impl;

import lesson11.interfaces.Context;
import lesson11.interfaces.ExecutionManager;
import lesson11.interfaces.ThreadPool;

public class ExecutionManagerImpl implements ExecutionManager {
    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        ThreadPool threadPool = new ThreadPoolImpl(callback, tasks);
        new Thread(threadPool::execute).start();
        return new ContextImpl(threadPool);
    }
}
