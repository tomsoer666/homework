package lesson11.interfaces;

import java.util.concurrent.atomic.AtomicInteger;

public interface ThreadPool {
    void execute();

    Thread[] getThreadPool();

    AtomicInteger getExceptionCount();
}
