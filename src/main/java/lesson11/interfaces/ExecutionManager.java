package lesson11.interfaces;

public interface ExecutionManager {
    public Context execute(Runnable callback, Runnable... tasks);
}
