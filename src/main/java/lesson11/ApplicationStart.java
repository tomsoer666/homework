package lesson11;

import lesson11.exceptions.FailedThreadException;
import lesson11.exceptions.NegativeValueException;
import lesson11.interfaces.Context;
import lesson11.interfaces.ExecutionManager;
import lesson11.interfaces.impl.ExecutionManagerImpl;
import lesson11.model.Task;

import java.util.Random;
import java.util.concurrent.Callable;

public class ApplicationStart {
    public static void main(String[] args) throws InterruptedException {
        // task 1
        Callable<Integer> callable = () -> {
            int value = new Random().nextInt();
            if (value < 0) {
                throw new NegativeValueException("Value must be positive");
            }
            return value;
        };
        Task<Integer> task = new Task<>(callable);
        for (int i = 0; i < 4; i++) {
            new Thread(() -> {
                try {
                    System.out.println(task.get() + " " + Thread.currentThread().getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
        //task 2
        Runnable done = () -> System.out.println("i'm done");
        Runnable[] tasks = new Runnable[4];
        for (int i = 0; i < tasks.length; i++) {
            int finalI = i;
            tasks[i] = () -> {
                if (finalI % 2 == 0) {
                    throw new FailedThreadException();
                }
                System.out.println("i'm working " + Thread.currentThread().getName());
            };
        }
        ExecutionManager manager = new ExecutionManagerImpl();
        Context context = manager.execute(done, tasks);
        Thread.sleep(1);
        context.interrupt();
        System.out.println(context.getCompletedTaskCount());
        System.out.println(context.getFailedTaskCount());
        System.out.println(context.getInterruptedTaskCount());
        System.out.println(context.isFinished());
    }
}
