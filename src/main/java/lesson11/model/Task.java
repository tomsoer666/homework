package lesson11.model;

import java.util.concurrent.Callable;

public class Task<T> {
    private final Callable<? extends T> callable;
    private T data;
    private RuntimeException exception;

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public synchronized T get() throws Exception {
        try {
            if (exception != null) {
                throw exception;
            }
            if (data == null) {
                data = callable.call();
            }
            return data;
        } catch (RuntimeException e) {
            exception = e;
            throw exception;
        }
    }
}
