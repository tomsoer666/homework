package lesson11.exceptions;

/*
    Exception caused while thread throw any exception
 */
public class FailedThreadException extends RuntimeException {
    public FailedThreadException() {
    }

    public FailedThreadException(String message) {
        super(message);
    }
}
