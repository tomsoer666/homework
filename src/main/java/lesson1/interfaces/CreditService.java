package lesson1.interfaces;

import lesson1.exception.ClientNotExistsException;
import lesson1.exception.CreditPaymentException;
import lesson1.models.Client;
import lesson1.models.Credit;
import lesson1.models.Transfer;

public interface CreditService {
    Credit obtainCredit(Client client, Long cash, Double interestRate,
                        Integer creditTerm, Long paymentAmount) throws ClientNotExistsException;

    Transfer repayCredit(Credit credit, Long paymentAmount) throws CreditPaymentException;
}
