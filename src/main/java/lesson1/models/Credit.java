package lesson1.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import lesson1.enums.CreditStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Credit {
    @Expose
    private Long id;
    private Client client;
    @Expose
    private Long cash;
    @Expose
    private CreditStatus creditStatus;
    @Expose
    private Double interestRate; //%
    @Expose
    private Long monthlyPayment;
    @Expose
    private Long totalPayout;
    @Expose
    private Integer creditTerm; //month
    @Expose
    private Long creditBalance;
    @Expose
    private List<Transfer> transfers;

    public Credit(Long id, Client client, Long cash, CreditStatus creditStatus, Double interestRate,
                  Long monthlyPayment, Long totalPayout, Integer creditTerm, Long creditBalance) {
        this.id = id;
        this.client = client;
        this.cash = cash;
        this.creditStatus = creditStatus;
        this.interestRate = interestRate;
        this.monthlyPayment = monthlyPayment;
        this.totalPayout = totalPayout;
        this.creditTerm = creditTerm;
        this.creditBalance = creditBalance;
        transfers = new ArrayList<>();
    }

    public Credit() {
    }

    public Long getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Long getCash() {
        return cash;
    }

    public CreditStatus getCreditStatus() {
        return creditStatus;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public Long getMonthlyPayment() {
        return monthlyPayment;
    }

    public Long getTotalPayout() {
        return totalPayout;
    }

    public Integer getCreditTerm() {
        return creditTerm;
    }

    public Long getCreditBalance() {
        return creditBalance;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setCreditStatus(CreditStatus creditStatus) {
        this.creditStatus = creditStatus;
    }

    public void changeCreditBalance(Long payment) {
        creditBalance += payment;
    }

    public void addTransfers(Transfer transfer) {
        transfers.add(transfer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return id.equals(credit.id) &&
                client.equals(credit.client) &&
                cash.equals(credit.cash) &&
                creditStatus == credit.creditStatus &&
                interestRate.equals(credit.interestRate) &&
                monthlyPayment.equals(credit.monthlyPayment) &&
                totalPayout.equals(credit.totalPayout) &&
                creditTerm.equals(credit.creditTerm) &&
                creditBalance.equals(credit.creditBalance) &&
                transfers.equals(credit.transfers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, cash, creditStatus, interestRate, monthlyPayment, totalPayout, creditTerm, creditBalance, transfers);
    }

    @Override
    public String toString() {
        com.google.gson.Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
