package lesson1.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Client {
    @Expose
    private Long id;
    @Expose
    private String fullName;
    @Expose
    private String secondName;
    @Expose
    private int age;
    @Expose
    private String email;
    @Expose
    private List<Credit> credits;

    public Client(Long id, String fullName, String secondName, int age, String email) {
        this.id = id;
        this.fullName = fullName;
        this.secondName = secondName;
        this.age = age;
        this.email = email;
        credits = new ArrayList<>();
    }

    public Client() {
    }

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public void addCredit(Credit credit) {
        credits.add(credit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return age == client.age &&
                id.equals(client.id) &&
                fullName.equals(client.fullName) &&
                secondName.equals(client.secondName) &&
                Objects.equals(email, client.email) &&
                Objects.equals(credits, client.credits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, secondName, age, email, credits);
    }

    @Override
    public String toString() {
        com.google.gson.Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
