package lesson1.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import lesson1.enums.TransferStatus;
import lesson1.enums.TransferType;

import java.util.Date;
import java.util.Objects;

public class Transfer {
    @Expose
    private Long id;
    private Client client;
    private Credit credit;
    @Expose
    private Long paymentAmount;
    @Expose
    private TransferType transferType;
    @Expose
    private Date transactionDate;
    @Expose
    private TransferStatus transferStatus;

    public Transfer(Long id, Client client, Credit credit, Long paymentAmount,
                    TransferType transferType, Date transactionDate) {
        this.id = id;
        this.client = client;
        this.credit = credit;
        this.paymentAmount = paymentAmount;
        this.transferType = transferType;
        this.transactionDate = transactionDate;
    }

    public Transfer() {
    }

    public Long getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Credit getCredit() {
        return credit;
    }

    public Long getPaymentAmount() {
        return paymentAmount;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public TransferStatus getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(TransferStatus transferStatus) {
        this.transferStatus = transferStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return id.equals(transfer.id) &&
                client.equals(transfer.client) &&
                credit.equals(transfer.credit) &&
                paymentAmount.equals(transfer.paymentAmount) &&
                transferType == transfer.transferType &&
                transactionDate.equals(transfer.transactionDate) &&
                transferStatus == transfer.transferStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, credit, paymentAmount, transferType, transactionDate, transferStatus);
    }

    @Override
    public String toString() {
        com.google.gson.Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(this);
    }
}
