package lesson1;

import lesson1.exception.ClientNotExistsException;
import lesson1.exception.CreditPaymentException;
import lesson1.interfaces.CreditService;
import lesson1.models.Client;
import lesson1.models.Credit;
import lesson1.service.CreditServiceImpl;

public class ApplicationStart {
    public static void main(String[] args) {
        Client client = new Client(1L, "Ilya", "Trukhov", 23,
                "trukhovilya@mail.ru");
        CreditService creditService = new CreditServiceImpl();
        try {
            Credit credit = creditService.obtainCredit(client, 10000L, 0.8, 12, 0L);
            creditService.repayCredit(credit, 2000L);
        } catch (ClientNotExistsException | CreditPaymentException e) {
            System.out.println(e);
        }
        System.out.println(client.toString());
    }
}
