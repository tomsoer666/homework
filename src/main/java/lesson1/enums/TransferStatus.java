package lesson1.enums;

public enum TransferStatus {
    TRANSACTION_ACCESS,
    TRANSACTION_DECLINE
}
