package lesson1.enums;

public enum TransferType {
    CREDIT_REQUEST,
    CREDIT_PAY
}
