package lesson1.enums;

public enum CreditStatus {
    ACTIVE,
    PAID
}
