package lesson1.service;

import lesson1.enums.CreditStatus;
import lesson1.enums.TransferStatus;
import lesson1.enums.TransferType;
import lesson1.exception.ClientNotExistsException;
import lesson1.exception.CreditPaymentException;
import lesson1.interfaces.CreditService;
import lesson1.models.Client;
import lesson1.models.Credit;
import lesson1.models.Transfer;

import java.util.Date;
import java.util.Random;

public class CreditServiceImpl implements CreditService {

    @Override
    public Credit obtainCredit(Client client, Long cash, Double interestRate,
                               Integer creditTerm, Long paymentAmount) throws ClientNotExistsException {
        if (client == null) {
            throw new ClientNotExistsException(TransferStatus.TRANSACTION_DECLINE);
        }
        Credit credit = calculateCredit(client, cash, interestRate, creditTerm, paymentAmount);
        Transfer transfer = new Transfer(new Random().nextLong(), client, credit, paymentAmount,
                TransferType.CREDIT_REQUEST, new Date(System.currentTimeMillis()));
        transfer.setTransferStatus(TransferStatus.TRANSACTION_ACCESS);
        credit.addTransfers(transfer);
        client.addCredit(credit);
        return credit;
    }

    @Override
    public Transfer repayCredit(Credit credit, Long paymentAmount) throws CreditPaymentException {
        if (credit.getCreditStatus() == CreditStatus.PAID) {
            throw new CreditPaymentException(TransferStatus.TRANSACTION_DECLINE);
        }
        creditPay(credit, paymentAmount);
        Transfer transfer = new Transfer(new Random().nextLong(), credit.getClient(), credit, paymentAmount,
                TransferType.CREDIT_PAY, new Date(System.currentTimeMillis()));
        transfer.setTransferStatus(TransferStatus.TRANSACTION_ACCESS);
        credit.addTransfers(transfer);
        return transfer;
    }

    private Credit calculateCredit(Client client, Long cash, Double interestRate, Integer creditTerm,
                                   Long paymentAmount) {
        Long totalPayout = (long) (cash / (1L + interestRate));
        Long monthlyPayment = (cash + totalPayout - paymentAmount) / creditTerm;
        return new Credit(new Random().nextLong(), client, cash, CreditStatus.ACTIVE, interestRate,
                monthlyPayment, totalPayout, creditTerm, paymentAmount);
    }

    private void creditPay(Credit credit, Long paymentAmount) {
        credit.changeCreditBalance(paymentAmount);
        if (credit.getCreditBalance() >= credit.getTotalPayout() + credit.getCash()) {
            credit.setCreditStatus(CreditStatus.PAID);
        }
    }
}
