package lesson1.exception;

import lesson1.enums.TransferStatus;

public class CreditPaymentException extends Exception {
    public CreditPaymentException(TransferStatus transferStatus) {
        super(transferStatus.toString() + " Client already pay credit");
    }
}
