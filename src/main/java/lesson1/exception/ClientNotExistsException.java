package lesson1.exception;

import lesson1.enums.TransferStatus;

public class ClientNotExistsException extends Exception {
    public ClientNotExistsException(TransferStatus transferStatus) {
        super(transferStatus.toString() + " Client not exists");
    }
}
