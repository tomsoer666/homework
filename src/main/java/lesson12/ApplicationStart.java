package lesson12;

import lesson12.service.ProducerController;

public class ApplicationStart {
    public static void main(String[] args) throws InterruptedException {
        ProducerController<String> producerController = new ProducerController<>();
        Runnable produceTask = () -> {
            try {
                producerController.produce(Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Runnable consumeTask = () -> {
            try {
                producerController.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                new Thread(consumeTask).start();
            }
            new Thread(produceTask).start();
        }
        Thread.sleep(1000);
    }
}
