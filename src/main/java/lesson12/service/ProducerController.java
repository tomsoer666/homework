package lesson12.service;

import lesson12.interfaces.Stack;
import lesson12.interfaces.impl.StackLIFO;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerController<T> {
    private final Stack<T> stack = new StackLIFO<>();
    private final LinkedBlockingQueue<T> producerQueue = new LinkedBlockingQueue<>();
    private List<Producer> producers;


    public ProducerController() {
        producers = Arrays.asList(new Producer(), new Producer(), new Producer());
        for (Producer producer : producers) {
            producer.start();
        }
    }

    public void produce(T value) throws InterruptedException {
        synchronized (producerQueue) {
            producerQueue.add(value);
            producerQueue.notify();
        }
    }


    public T consume() throws InterruptedException {
        synchronized (this) {
            while (stack.size() == 0) {
                wait();
            }
            T result = stack.pop();
            System.out.println("Poped " + Thread.currentThread().getName());
            notifyAll();
            return result;
        }
    }

    private class Producer extends Thread {
        @Override
        public void run() {
            T value;
            while (true) {
                synchronized (producerQueue) {
                    while (producerQueue.isEmpty()) {
                        try {
                            producerQueue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Ошибка во время ожидания потока " + e.getMessage());
                        }
                    }
                    value = producerQueue.poll();
                }

                try {
                    System.out.println(Thread.currentThread().getName() + " work");
                    stack.push(value);
                    System.out.println("Pushed " + Thread.currentThread().getName());
                } catch (RuntimeException e) {
                    System.out.println("Пул потоков был непредвидено прерван " + e.getMessage());
                }
            }
        }
    }
}
