package lesson12.interfaces;

public interface Stack<T> {
    void push(T t);

    T pop();

    int size();
}
