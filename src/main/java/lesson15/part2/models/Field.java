package lesson15.part2.models;

public class Field {
    private final int positionX;
    private final int positionY;

    public Field(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }
}
