package lesson15.part2.models;

import lesson15.part2.enums.Orientation;

public class Tractor {
    private int positionX;
    private int positionY;
    private Orientation orientation;

    public Tractor() {
        positionX = 0;
        positionY = 0;
        orientation = Orientation.NORTH;
    }

    public Tractor(int positionX, int positionY, Orientation orientation) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.orientation = orientation;
    }

    public int getPositionX() {
        return positionX;
    }

    public void incrementPositionX() {
        positionX++;
    }

    public void decrementPositionX() {
        positionX--;
    }

    public int getPositionY() {
        return positionY;
    }

    public void incrementPositionY() {
        positionY++;
    }

    public void decrementPositionY() {
        positionY--;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}
