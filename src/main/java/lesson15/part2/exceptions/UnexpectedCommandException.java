package lesson15.part2.exceptions;

public class UnexpectedCommandException extends RuntimeException {
    public UnexpectedCommandException() {
    }

    public UnexpectedCommandException(String message) {
        super(message);
    }
}
