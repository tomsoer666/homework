package lesson15.part2.exceptions;

public class TractorInDitchException extends RuntimeException {
    public TractorInDitchException() {
    }

    public TractorInDitchException(String message) {
        super(message);
    }
}
