package lesson15.part2.enums;

public enum Command {
    FORWARD,
    TURN_CLOCKWISE
}
