package lesson15.part2.enums;

public enum Orientation {
    NORTH,
    WEST,
    SOUTH,
    EAST
}
