package lesson15.part2.interfaces;

import lesson15.part2.enums.Command;

public interface TractorService {
    void move(Command command);
}
