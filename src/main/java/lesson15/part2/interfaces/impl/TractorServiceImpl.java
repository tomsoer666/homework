package lesson15.part2.interfaces.impl;

import lesson15.part2.enums.Command;
import lesson15.part2.enums.Orientation;
import lesson15.part2.exceptions.TractorInDitchException;
import lesson15.part2.exceptions.UnexpectedCommandException;
import lesson15.part2.interfaces.TractorService;
import lesson15.part2.models.Field;
import lesson15.part2.models.Tractor;

public class TractorServiceImpl implements TractorService {
    private final Field field;
    private final Tractor tractor;

    public TractorServiceImpl(Field field, Tractor tractor) {
        this.field = field;
        this.tractor = tractor;
    }

    @Override
    public void move(Command command) {
        switch (command) {
            case FORWARD:
                moveForwards();
                break;
            case TURN_CLOCKWISE:
                turnClockwise();
                break;
            default:
                throw new UnexpectedCommandException("Invalid command");
        }
    }

    private void moveForwards() {
        switch (tractor.getOrientation()) {
            case NORTH:
                tractor.incrementPositionY();
                break;
            case EAST:
                tractor.incrementPositionX();
                break;
            case SOUTH:
                tractor.decrementPositionY();
                break;
            case WEST:
                tractor.decrementPositionX();
                break;
            default:
                break;
        }
        checkTractorPositionOnField();
    }

    private void checkTractorPositionOnField() {
        if (tractor.getPositionX() > field.getPositionX() || tractor.getPositionY() > field.getPositionY()) {
            throw new TractorInDitchException("Tractor out of the field");
        }
    }

    private void turnClockwise() {
        switch (tractor.getOrientation()) {
            case NORTH:
                tractor.setOrientation(Orientation.EAST);
                break;
            case EAST:
                tractor.setOrientation(Orientation.SOUTH);
                break;
            case SOUTH:
                tractor.setOrientation(Orientation.WEST);
                break;
            case WEST:
                tractor.setOrientation(Orientation.NORTH);
                break;
            default:
                break;
        }
    }
}
