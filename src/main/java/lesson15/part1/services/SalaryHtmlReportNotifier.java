package lesson15.part1.services;

import lesson15.part1.models.Result;
import lesson15.part1.models.SalaryPayments;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.sql.*;

public class SalaryHtmlReportNotifier {
    private static final String SQL_QUERY = "SELECT emp.id AS emp_id, " +
            "emp.name AS amp_name, SUM(salary) AS salary " +
            "FROM employee emp " +
            "LEFT JOIN salary_payments sp " +
            "ON emp.id = sp.employee_id " +
            "WHERE emp.department_id = ? " +
            "AND sp.date >= ? " +
            "AND sp.date <= ? " +
            "GROUP BY emp.id, emp.name";
    private static final String HTML_INITIAL_BODY = "<html><body><table><tr><td>Employee</td><td>Salary</td></tr>";
    private static final String ROW_START = "<tr>";
    private static final String COLUMN_START = "<td>";
    private static final String COLUMN_END = "</td>";
    private static final String ROW_END = "</tr>";
    private static final String TOTAL_TABLE = "<tr><td>Total</td><td>";
    private static final String HTML_END_BODY = "</td></tr></table></body></html>";
    private static final String HOST = "mail.google.com";
    private static final String SALARY_REPORT = "Monthly department salary report";

    private final Connection connection;

    public SalaryHtmlReportNotifier(Connection databaseConnection) {
        this.connection = databaseConnection;
    }

    public void generateAndSendHtmlSalaryReport(SalaryPayments salaryPayments, String recipients) {
        try (PreparedStatement ps = connection.prepareStatement(SQL_QUERY)) {
            // inject parameters to sql
            injectParameters(ps, salaryPayments);
            // execute query and get the results
            ResultSet results = ps.executeQuery();
            // generate html salary report
            StringBuilder resultingHtml = generateHtmlSalaryReport(results);
            // send html salary report
            sendHtmlSalaryReport(resultingHtml, recipients);
        } catch (SQLException | MessagingException e) {
            System.err.println(e.getMessage());
        }
    }

    private void sendHtmlSalaryReport(StringBuilder resultingHtml, String recipients)
            throws MessagingException {
        // now when the report is built we need to send it to the recipients list
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        // we're going to use google mail to send this message
        mailSender.setHost(HOST);
        // construct the message
        MimeMessage message = mailSender.createMimeMessage();
        // configure message
        configureMessage(resultingHtml, recipients, message);
        // send the message
        mailSender.send(message);
    }

    private void configureMessage(StringBuilder resultingHtml, String recipients, MimeMessage message)
            throws MessagingException {
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(recipients);
        // setting message text, last parameter 'true' says that it is HTML format
        helper.setText(resultingHtml.toString(), true);
        helper.setSubject(SALARY_REPORT);
    }

    private StringBuilder generateHtmlSalaryReport(ResultSet results)
            throws SQLException {
        // create a StringBuilder holding a resulting html
        StringBuilder resultingHtml = new StringBuilder();
        resultingHtml.append(HTML_INITIAL_BODY);
        Result data = new Result();
        double totals = 0;
        while (results.next()) {
            // process each row of query results
            setResult(results, data);
            appendResult(data, resultingHtml);
            totals += data.getSalary(); // add salary to totals
        }
        resultingHtml.append(TOTAL_TABLE).append(totals);
        resultingHtml.append(HTML_END_BODY);
        return resultingHtml;
    }

    private void appendResult(Result data, StringBuilder resultingHtml) {
        resultingHtml.append(ROW_START); // add row start tag
        resultingHtml.append(COLUMN_START).append(data.getEmployerName()).append(COLUMN_END); // appending employee name
        resultingHtml.append(COLUMN_START).append(data.getSalary()).append(COLUMN_END); // appending employee salary for period
        resultingHtml.append(ROW_END); // add row end tag
    }

    private void setResult(ResultSet results, Result data)
            throws SQLException {
        data.setEmployerName(results.getString("emp_name"));
        data.setSalary(results.getDouble("salary"));
    }

    private void injectParameters(PreparedStatement ps, SalaryPayments salaryPayments)
            throws SQLException {
        ps.setString(1, salaryPayments.getEmployee().getDepartmentId());
        ps.setDate(2, new Date(salaryPayments.getDateFrom().toEpochDay()));
        ps.setDate(3, new Date(salaryPayments.getDateTo().toEpochDay()));
    }
}
