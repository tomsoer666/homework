package lesson13.server.interfaces.impl;

import lesson13.server.interfaces.Server;
import lesson13.server.service.ServerService;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerImpl implements Server {

    @Override
    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            System.out.println("Server started.");
            while (true) {
                Thread.sleep(2000);
                Socket clientSocket = serverSocket.accept();
                new Thread(new ServerStartTask(clientSocket)).start();
            }
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Server closed by error.");
            System.err.println(e.getMessage());
        }
    }

    private static class ServerStartTask implements Runnable {
        private final Socket clientSocket;

        public ServerStartTask(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                new ServerService().startServer(clientSocket);
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
