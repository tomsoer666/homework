package lesson13.server;

import lesson13.server.interfaces.Server;
import lesson13.server.interfaces.impl.ServerImpl;

public class ServerApplication {
    public static void main(String[] args) {
        Server server = new ServerImpl();
        server.start();
    }
}
