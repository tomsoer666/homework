package lesson13.server.service;

import lesson13.server.models.MessageType;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerService {
    private static final List<String> LOGINS = Arrays.asList("Ilya", "Sasha", "Petya");
    private static final Map<String, Socket> USERS = new HashMap<>();
    private String username;

    public ServerService() {
    }

    private void invalidAuthorizedUserMessage(Socket clientSocket) throws IOException {
        String message = "User already authorized!";
        List<Socket> sockets = new ArrayList<>(USERS.values());
        for (Socket socket : sockets) {
            if (socket.getPort() == clientSocket.getPort()) {
                message = "You already authorized!";
            }
        }
        sendMessage(clientSocket, message);
    }

    private boolean sendMessageToUser(String username, String userMessage) {
        AtomicBoolean isSend = new AtomicBoolean(false);
        USERS.forEach((k, v) -> {
            if (userMessage.startsWith("@" + k)) {
                String message = username + " >>" + userMessage.replace("@" + k, "");
                try {
                    sendMessage(v, message);
                    isSend.set(true);
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        });
        return isSend.get();
    }

    private boolean isAuthorizedUser(Socket clientSocket, String user) {
        List<Socket> sockets = new ArrayList<>(USERS.values());
        for (Socket socket : sockets) {
            if (socket.getPort() == clientSocket.getPort()) {
                return true;
            }
        }
        return USERS.containsKey(user);
    }

    private void invalidUserMessage(Socket clientSocket, String user) throws IOException {
        String message = "User \"" + user + "\" not exist!";
        sendMessage(clientSocket, message);
    }

    private void welcomeMessage(String user) {
        String message = "Meet new user! Welcome " + user + "!";
        USERS.forEach((k, v) -> {
            try {
                sendMessage(v, message);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        });
        System.out.println(message);
    }

    private boolean isUser(String user) {
        for (String login : LOGINS) {
            if (login.equals(user)) {
                return true;
            }
        }
        return false;
    }

    private MessageType checkMessageType(String message) {
        if (message.startsWith("@")) {
            return MessageType.MESSAGE;
        } else if (message.startsWith("exit")) {
            return MessageType.DISCONNECT;
        } else return MessageType.AUTHORIZATION;
    }

    private void sendMessage(Socket clientSocket, String message) throws IOException {
        OutputStream outputStream = clientSocket.getOutputStream();
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        bufferedWriter.write(message + "\n");
        bufferedWriter.flush();
    }

    private String readMessage(Socket clientSocket) throws IOException {
        InputStream inputStream = clientSocket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String message;
        while (!bufferedReader.ready())
            Thread.yield();
        message = bufferedReader.readLine();
        return message;
    }

    private void userNotInSystemMessage(Socket clientSocket) throws IOException {
        String message = "User not in system!";
        sendMessage(clientSocket, message);
    }

    private void invalidUnAuthorizedUserMessage(Socket clientSocket) throws IOException {
        String message = "You not authorized! Enter in system.";
        sendMessage(clientSocket, message);
    }

    public void startServer(Socket clientSocket) throws IOException, InterruptedException {
        String message;
        while (!clientSocket.isClosed()) {
            Thread.sleep(2000);
            message = readMessage(clientSocket);
            switch (checkMessageType(message)) {
                case AUTHORIZATION:
                    if (isUser(message)) {
                        if (isAuthorizedUser(clientSocket, message)) {
                            invalidAuthorizedUserMessage(clientSocket);
                        } else {
                            USERS.putIfAbsent(message, clientSocket);
                            setUsername(message);
                            welcomeMessage(message);
                        }
                    } else {
                        invalidUserMessage(clientSocket, message);
                    }
                    break;
                case MESSAGE:
                    if (isAuthorizedUser(clientSocket, message)) {
                        if (!sendMessageToUser(getUsername(), message)) {
                            userNotInSystemMessage(clientSocket);
                        }
                    } else {
                        invalidUnAuthorizedUserMessage(clientSocket);
                    }
                    break;
                case DISCONNECT:
                    disconnectUser(clientSocket);
                    break;
                default:
                    break;
            }
        }
    }

    private void disconnectUser(Socket clientSocket) throws IOException {
        if (USERS.containsValue(clientSocket)) {
            USERS.remove(getUsername(), clientSocket);
        }
        String message = "exit";
        sendMessage(clientSocket, message);
        clientSocket.close();
        System.out.println("User disconnected.");
    }

    private String getUsername() {
        return username;
    }

    private void setUsername(String username) {
        this.username = username;
    }
}
