package lesson13.server.models;

public enum MessageType {
    AUTHORIZATION,
    MESSAGE,
    DISCONNECT
}
