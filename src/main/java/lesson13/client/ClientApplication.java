package lesson13.client;

import lesson13.client.interfaces.Client;
import lesson13.client.interfaces.impl.ClientImpl;

public class ClientApplication {
    public static void main(String[] args) {
        Client client = new ClientImpl();
        client.connect();
    }
}
