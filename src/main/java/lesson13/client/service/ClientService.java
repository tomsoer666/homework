package lesson13.client.service;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientService {
    public static final String EXIT = "exit";
    private static final Scanner CHAT = new Scanner(System.in);

    private ClientService() {

    }

    public static void sendMessage(Socket clientSocket) throws IOException {
        OutputStream outputStream = clientSocket.getOutputStream();
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        String message = "";
        while (!message.equals(EXIT + "\n")) {
            message = CHAT.nextLine() + "\n";
            bufferedWriter.write(message);
            bufferedWriter.flush();
        }
    }

    public static void readMessage(Socket clientSocket) throws IOException {
        try (InputStream inputStream = clientSocket.getInputStream();
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String message = "";
            do {
                Thread.sleep(2000);
                if (bufferedReader.ready()) {
                    message = bufferedReader.readLine();
                    System.out.println(message);
                }
            } while (!message.equals(EXIT));
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }
    }
}
