package lesson13.client.interfaces.impl;

import lesson13.client.interfaces.Client;
import lesson13.client.service.ClientService;

import java.io.IOException;
import java.net.Socket;

public class ClientImpl implements Client {
    private static final String PORT_NAME = "localhost";
    private static final int PORT = 8080;

    @Override
    public void connect() {
        try (Socket clientSocket = new Socket(PORT_NAME, PORT)) {
            Thread writeThread = new Thread(new ClientWriteMessageTask(clientSocket));
            Thread readThread = new Thread(new ClientReadMessageTask(clientSocket));
            writeThread.start();
            readThread.start();
            System.out.println("Connection started.");
            while (writeThread.isAlive() || readThread.isAlive()) {
                Thread.sleep(2000);
            }
            System.out.println("Connection closed.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Connection closed by error.");
            System.err.println(e.getMessage());
        }
    }

    private static class ClientWriteMessageTask implements Runnable {
        private final Socket clientSocket;

        public ClientWriteMessageTask(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                ClientService.sendMessage(clientSocket);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private static class ClientReadMessageTask implements Runnable {
        private final Socket clientSocket;

        public ClientReadMessageTask(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try {
                ClientService.readMessage(clientSocket);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
