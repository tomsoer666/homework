package lesson9.stream;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Streams<T> {
    private Consumer<Consumer<T>> currentProcessor;

    private Streams(Consumer<Consumer<T>> currentProcessor) {
        this.currentProcessor = currentProcessor;
    }

    public static <T> Streams<T> of(List<T> list) {
        Consumer<Consumer<T>> nextProcessor = processor -> {
            for (T item : list) {
                processor.accept(item);
            }
        };
        return new Streams<>(nextProcessor);
    }

    public <N> Streams<N> transform(Function<T, N> function) {
        Consumer<Consumer<N>> nextProcessor = processor ->
                currentProcessor.accept(item -> processor.accept(function.apply(item)));
        return new Streams<>(nextProcessor);
    }

    public Streams<T> filter(Predicate<T> predicate) {
        Consumer<Consumer<T>> nextProcessor = processor ->
                currentProcessor.accept(item -> {
                    if (predicate.test(item)) {
                        processor.accept(item);
                    }
                });
        return new Streams<>(nextProcessor);
    }

    public <K, V> Map<K, V> toMap(Function<T, K> keyFunction, Function<T, V> valueFunction) {
        Map<K, V> result = new HashMap<>();
        currentProcessor.accept(item -> {
            K key = keyFunction.apply(item);
            result.putIfAbsent(key, valueFunction.apply(item));
        });
        return result;
    }
}
