package lesson9;

import lesson9.model.Person;
import lesson9.stream.Streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ApplicationStart {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(new Person("Alex", 15),
                new Person("Peter", 34),
                new Person("Masha", 31));
        Map<String, Integer> map = Streams.of(people)
                .filter(p -> p.getAge() > 20).
                        transform(p -> new Person(p.getName(), p.getAge() + 30))
                .toMap(Person::getName, Person::getAge);
        System.out.println(map);
    }
}
