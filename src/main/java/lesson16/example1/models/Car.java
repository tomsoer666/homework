package lesson16.example1.models;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.Wheels;

public class Car extends Vehicle {

    public Car(Chassis chassis, Engine engine, Wheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Car{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
