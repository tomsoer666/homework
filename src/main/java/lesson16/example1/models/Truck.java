package lesson16.example1.models;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.Wheels;

public class Truck extends Vehicle {

    public Truck(Chassis chassis, Engine engine, Wheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
