package lesson16.example1.models;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.Wheels;

public class Van extends Vehicle {

    public Van(Chassis chassis, Engine engine, Wheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
