package lesson16.example1.models;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.Wheels;

public abstract class Vehicle {
    private final Chassis chassis;
    private final Engine engine;
    private final Wheels wheels;

    protected Vehicle(Chassis chassis, Engine engine, Wheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public Engine getEngine() {
        return engine;
    }

    public Wheels getWheels() {
        return wheels;
    }
}
