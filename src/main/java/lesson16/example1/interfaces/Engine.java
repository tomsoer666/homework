package lesson16.example1.interfaces;

public interface Engine {
    String getEngineParts();
}
