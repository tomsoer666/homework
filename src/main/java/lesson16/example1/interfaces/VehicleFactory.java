package lesson16.example1.interfaces;

public interface VehicleFactory {
    Chassis createChassis();

    Engine createEngine();

    Wheels createWheels();
}
