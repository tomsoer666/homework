package lesson16.example1.interfaces;

public interface Chassis {
    String getChassisParts();
}
