package lesson16.example1.interfaces.impl.engines;

import lesson16.example1.interfaces.Engine;

public class CarEngine implements Engine {
    private String parts;

    @Override
    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
