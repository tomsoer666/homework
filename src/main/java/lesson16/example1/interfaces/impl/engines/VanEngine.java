package lesson16.example1.interfaces.impl.engines;

import lesson16.example1.interfaces.Engine;

public class VanEngine implements Engine {
    private String parts;

    @Override
    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
