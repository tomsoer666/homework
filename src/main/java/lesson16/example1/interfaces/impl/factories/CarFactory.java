package lesson16.example1.interfaces.impl.factories;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.VehicleFactory;
import lesson16.example1.interfaces.Wheels;
import lesson16.example1.interfaces.impl.chassis.CarChassis;
import lesson16.example1.interfaces.impl.engines.CarEngine;
import lesson16.example1.interfaces.impl.wheels.CarWheels;

public class CarFactory implements VehicleFactory {
    @Override
    public Chassis createChassis() {
        return new CarChassis();
    }

    @Override
    public Engine createEngine() {
        return new CarEngine();
    }

    @Override
    public Wheels createWheels() {
        return new CarWheels();
    }
}
