package lesson16.example1.interfaces.impl.factories;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.VehicleFactory;
import lesson16.example1.interfaces.Wheels;
import lesson16.example1.interfaces.impl.chassis.VanChassis;
import lesson16.example1.interfaces.impl.engines.VanEngine;
import lesson16.example1.interfaces.impl.wheels.VanWheels;

public class VanFactory implements VehicleFactory {
    @Override
    public Chassis createChassis() {
        return new VanChassis();
    }

    @Override
    public Engine createEngine() {
        return new VanEngine();
    }

    @Override
    public Wheels createWheels() {
        return new VanWheels();
    }
}
