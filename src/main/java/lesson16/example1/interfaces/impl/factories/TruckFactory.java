package lesson16.example1.interfaces.impl.factories;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.VehicleFactory;
import lesson16.example1.interfaces.Wheels;
import lesson16.example1.interfaces.impl.chassis.TruckChassis;
import lesson16.example1.interfaces.impl.engines.TruckEngine;
import lesson16.example1.interfaces.impl.wheels.TruckWheels;

public class TruckFactory implements VehicleFactory {
    @Override
    public Chassis createChassis() {
        return new TruckChassis();
    }

    @Override
    public Engine createEngine() {
        return new TruckEngine();
    }

    @Override
    public Wheels createWheels() {
        return new TruckWheels();
    }
}
