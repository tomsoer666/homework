package lesson16.example1.interfaces.impl.chassis;

import lesson16.example1.interfaces.Chassis;

public class TruckChassis implements Chassis {
    private String parts;

    @Override
    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
