package lesson16.example1.interfaces.impl.wheels;

import lesson16.example1.interfaces.Wheels;

public class VanWheels implements Wheels {
    private String parts;

    @Override
    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
