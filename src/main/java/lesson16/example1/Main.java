package lesson16.example1;

import lesson16.example1.interfaces.Chassis;
import lesson16.example1.interfaces.Engine;
import lesson16.example1.interfaces.VehicleFactory;
import lesson16.example1.interfaces.Wheels;
import lesson16.example1.interfaces.impl.factories.CarFactory;
import lesson16.example1.interfaces.impl.factories.TruckFactory;
import lesson16.example1.interfaces.impl.factories.VanFactory;
import lesson16.example1.models.Car;
import lesson16.example1.models.Truck;
import lesson16.example1.models.Van;
import lesson16.example1.models.Vehicle;

public class Main {
    public static void main(String[] args) {
        Vehicle car = createCar(new CarFactory());
        System.out.println(car);

        Vehicle van = createVan(new VanFactory());
        System.out.println(van);

        Vehicle truck = createTruck(new TruckFactory());
        System.out.println(truck);
    }

    private static Vehicle createCar(VehicleFactory vehicleFactory) {
        Chassis chassis = vehicleFactory.createChassis();
        Engine engine = vehicleFactory.createEngine();
        Wheels wheels = vehicleFactory.createWheels();
        return new Car(chassis, engine, wheels);
    }

    private static Vehicle createTruck(VehicleFactory vehicleFactory) {
        Chassis chassis = vehicleFactory.createChassis();
        Engine engine = vehicleFactory.createEngine();
        Wheels wheels = vehicleFactory.createWheels();
        return new Truck(chassis, engine, wheels);
    }

    private static Vehicle createVan(VehicleFactory vehicleFactory) {
        Chassis chassis = vehicleFactory.createChassis();
        Engine engine = vehicleFactory.createEngine();
        Wheels wheels = vehicleFactory.createWheels();
        return new Van(chassis, engine, wheels);
    }
}
