package lesson16.example4.singleton;

public class Database {

    private static Database instance;
    private final String address;

    private Database(String address) {
        this.address = address;
    }

    public static Database getInstance(String address) {
        if (instance == null) {
            instance = new Database(address);
        }
        return instance;
    }

    public String getAddress() {
        return address;
    }

    public String getData(long id) {
        // implementation
        return "some value";
    }

    public void saveData(String data, long id) {
        // implementation
    }
}
