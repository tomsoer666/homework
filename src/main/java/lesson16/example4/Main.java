package lesson16.example4;

import lesson16.example4.singleton.Database;

public class Main {

    private static final String DATABASE_ADDRESS = "some address";
    private static final String DATABASE_WRONG_ADDRESS = "bad address";
    private static Database database = Database.getInstance(DATABASE_ADDRESS);

    public static void main(String[] args) {
        someMethod1();
        someMethod2();
        someMethod3();
    }

    public static void someMethod1() {
        System.out.println(database.getData(1L));
        // что-то делает с database
    }

    public static void someMethod2() {
        database.saveData("data", 1L);
        // что-то делает с database
    }

    public static void someMethod3() {
        database = Database.getInstance(DATABASE_WRONG_ADDRESS);
        System.out.println(database.getAddress());
        // что-то делает с database
    }
}
