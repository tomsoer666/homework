package lesson16.example5.services;

import lesson16.example5.composite.Merchandise;

import java.util.List;

public class Cashier {
    public double calculateSum(List<Merchandise> purchases) {
        double resultSum = 0d;
        for (Merchandise purchase : purchases) {
            resultSum += purchase.getPrice();
        }
        return resultSum;
    }
}
