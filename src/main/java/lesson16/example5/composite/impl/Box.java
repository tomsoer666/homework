package lesson16.example5.composite.impl;

import lesson16.example5.composite.Merchandise;

import java.util.ArrayList;
import java.util.List;

public class Box implements Merchandise {

    // в списке может храниться как Box так и Product
    private final List<Merchandise> purchases = new ArrayList<>();

    public void addPurchase(Merchandise purchase) {
        purchases.add(purchase);
    }

    public List<Merchandise> getPurchases() {
        return purchases;
    }

    @Override
    public double getPrice() {
        double price = 0d;
        for (Merchandise merchandise : purchases) {
            price += merchandise.getPrice();
        }
        return price;
    }
}
