package lesson16.example5.composite.impl;

import lesson16.example5.composite.Merchandise;

public class Product implements Merchandise {

    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
