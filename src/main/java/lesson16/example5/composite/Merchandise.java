package lesson16.example5.composite;

public interface Merchandise {
    double getPrice();
}
