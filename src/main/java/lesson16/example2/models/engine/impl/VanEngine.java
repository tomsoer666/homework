package lesson16.example2.models.engine.impl;

import lesson16.example2.models.engine.Engine;

public class VanEngine implements Engine {

    private final String parts;

    public VanEngine() {
        this.parts = "parts of van engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
