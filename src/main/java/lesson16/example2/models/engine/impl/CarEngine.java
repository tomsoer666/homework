package lesson16.example2.models.engine.impl;


import lesson16.example2.models.engine.Engine;

public class CarEngine implements Engine {

    private final String parts;

    public CarEngine() {
        this.parts = "parts of car engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
