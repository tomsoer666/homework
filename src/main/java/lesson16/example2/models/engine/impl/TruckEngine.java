package lesson16.example2.models.engine.impl;

import lesson16.example2.models.engine.Engine;

public class TruckEngine implements Engine {

    private final String parts;

    public TruckEngine() {
        this.parts = "parts of truck engine";
    }

    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
