package lesson16.example2.models.engine;

public interface Engine {

    String getEngineParts();
}
