package lesson16.example2.models.steering.impl;

import lesson16.example2.models.steering.Steering;

public class StandardSteering implements Steering {

    @Override
    public String toString() {
        return "StandardSteering{}";
    }
}
