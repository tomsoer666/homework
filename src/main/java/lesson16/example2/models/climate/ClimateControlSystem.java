package lesson16.example2.models.climate;

public class ClimateControlSystem {

    private final String parts;

    public ClimateControlSystem() {
        this.parts = "parts of climate control system";
    }

    public String getParts() {
        return parts;
    }
}
