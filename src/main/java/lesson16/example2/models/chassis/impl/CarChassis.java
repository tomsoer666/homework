package lesson16.example2.models.chassis.impl;

import lesson16.example2.models.chassis.Chassis;

public class CarChassis implements Chassis {

    private final String parts;

    public CarChassis() {
        this.parts = "parts of car chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
