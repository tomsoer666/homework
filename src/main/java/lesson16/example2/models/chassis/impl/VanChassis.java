package lesson16.example2.models.chassis.impl;

import lesson16.example2.models.chassis.Chassis;

public class VanChassis implements Chassis {

    private final String parts;

    public VanChassis() {
        this.parts = "parts of van chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
