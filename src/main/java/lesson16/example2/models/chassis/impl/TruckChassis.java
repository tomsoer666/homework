package lesson16.example2.models.chassis.impl;

import lesson16.example2.models.chassis.Chassis;

public class TruckChassis implements Chassis {

    private final String parts;

    public TruckChassis() {
        this.parts = "parts of truck chassis";
    }

    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
