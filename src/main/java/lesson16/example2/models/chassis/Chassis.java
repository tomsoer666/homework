package lesson16.example2.models.chassis;

public interface Chassis {
    String getChassisParts();
}
