package lesson16.example2.models.vehicle;

import lesson16.example2.models.brakes.ABS;
import lesson16.example2.models.chassis.Chassis;
import lesson16.example2.models.climate.AirConditioning;
import lesson16.example2.models.climate.ClimateControlSystem;
import lesson16.example2.models.climate.SeatHeating;
import lesson16.example2.models.engine.Engine;
import lesson16.example2.models.steering.Steering;
import lesson16.example2.models.wheels.Wheels;

public abstract class Vehicle {

    private Chassis chassis;
    private Engine engine;
    private Wheels wheels;
    private Steering steering;
    private ClimateControlSystem climateControlSystem;
    private AirConditioning airConditioning;
    private SeatHeating seatHeating;
    private ABS abs;

    public Chassis getChassis() {
        return chassis;
    }

    public void setChassis(Chassis chassis) {
        this.chassis = chassis;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    public Steering getSteering() {
        return steering;
    }

    public void setSteering(Steering steering) {
        this.steering = steering;
    }

    public ClimateControlSystem getClimateControlSystem() {
        return climateControlSystem;
    }

    public void setClimateControlSystem(ClimateControlSystem climateControlSystem) {
        this.climateControlSystem = climateControlSystem;
    }

    public AirConditioning getAirConditioning() {
        return airConditioning;
    }

    public void setAirConditioning(AirConditioning airConditioning) {
        this.airConditioning = airConditioning;
    }

    public SeatHeating getSeatHeating() {
        return seatHeating;
    }

    public void setSeatHeating(SeatHeating seatHeating) {
        this.seatHeating = seatHeating;
    }

    public ABS getAbs() {
        return abs;
    }

    public void setAbs(ABS abs) {
        this.abs = abs;
    }
}
