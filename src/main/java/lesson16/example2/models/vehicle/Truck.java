package lesson16.example2.models.vehicle;

public class Truck extends Vehicle {

    public Truck() {
        super();
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }
}
