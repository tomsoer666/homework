package lesson16.example2.models.wheels.impl;

import lesson16.example2.models.wheels.Wheels;

public class CarWheels implements Wheels {

    private final String parts;

    public CarWheels() {
        this.parts = "parts of car wheels";
    }

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
