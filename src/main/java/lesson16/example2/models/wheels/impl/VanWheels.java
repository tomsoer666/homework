package lesson16.example2.models.wheels.impl;

import lesson16.example2.models.wheels.Wheels;

public class VanWheels implements Wheels {

    private final String parts;

    public VanWheels() {
        this.parts = "parts of van wheels";
    }

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
