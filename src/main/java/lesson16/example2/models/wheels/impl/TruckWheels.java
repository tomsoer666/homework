package lesson16.example2.models.wheels.impl;

import lesson16.example2.models.wheels.Wheels;

public class TruckWheels implements Wheels {

    private final String parts;

    public TruckWheels() {
        this.parts = "parts of truck wheels";
    }

    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
