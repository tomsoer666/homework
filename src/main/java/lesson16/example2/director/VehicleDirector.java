package lesson16.example2.director;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.models.vehicle.Vehicle;

public interface VehicleDirector {
    Vehicle build(VehicleBuilder vehicleBuilder);
}
