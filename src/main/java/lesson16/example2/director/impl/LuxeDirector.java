package lesson16.example2.director.impl;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.director.VehicleDirector;
import lesson16.example2.models.vehicle.Vehicle;

public class LuxeDirector implements VehicleDirector {

    @Override
    public Vehicle build(VehicleBuilder vehicleBuilder) {
        vehicleBuilder.buildChassis();
        vehicleBuilder.buildWheels();
        vehicleBuilder.buildEngine();
        vehicleBuilder.buildSteering();
        vehicleBuilder.buildAbs();
        vehicleBuilder.buildAirConditioning();
        vehicleBuilder.buildClimateControlSystem();
        vehicleBuilder.buildSeatHeating();
        return vehicleBuilder.getVehicle();
    }
}
