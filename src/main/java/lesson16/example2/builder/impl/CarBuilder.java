package lesson16.example2.builder.impl;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.models.brakes.ABS;
import lesson16.example2.models.chassis.impl.CarChassis;
import lesson16.example2.models.climate.AirConditioning;
import lesson16.example2.models.climate.ClimateControlSystem;
import lesson16.example2.models.climate.SeatHeating;
import lesson16.example2.models.engine.impl.CarEngine;
import lesson16.example2.models.steering.impl.PowerSteering;
import lesson16.example2.models.vehicle.Car;
import lesson16.example2.models.vehicle.Vehicle;
import lesson16.example2.models.wheels.impl.CarWheels;

public class CarBuilder implements VehicleBuilder {
    private final Vehicle vehicle = new Car();

    @Override
    public void buildAbs() {
        vehicle.setAbs(new ABS());
    }

    @Override
    public void buildChassis() {
        vehicle.setChassis(new CarChassis());
    }

    @Override
    public void buildClimateControlSystem() {
        vehicle.setClimateControlSystem(new ClimateControlSystem());
    }

    @Override
    public void buildAirConditioning() {
        vehicle.setAirConditioning(new AirConditioning());
    }

    @Override
    public void buildSeatHeating() {
        vehicle.setSeatHeating(new SeatHeating());
    }

    @Override
    public void buildEngine() {
        vehicle.setEngine(new CarEngine());
    }

    @Override
    public void buildSteering() {
        vehicle.setSteering(new PowerSteering());
    }

    @Override
    public void buildWheels() {
        vehicle.setWheels(new CarWheels());
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }
}
