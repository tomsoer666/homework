package lesson16.example2.builder.impl;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.models.brakes.ABS;
import lesson16.example2.models.chassis.impl.TruckChassis;
import lesson16.example2.models.climate.AirConditioning;
import lesson16.example2.models.climate.ClimateControlSystem;
import lesson16.example2.models.climate.SeatHeating;
import lesson16.example2.models.engine.impl.TruckEngine;
import lesson16.example2.models.steering.impl.PowerSteering;
import lesson16.example2.models.vehicle.Truck;
import lesson16.example2.models.vehicle.Vehicle;
import lesson16.example2.models.wheels.impl.TruckWheels;

public class TruckBuilder implements VehicleBuilder {
    private final Vehicle vehicle = new Truck();

    @Override
    public void buildAbs() {
        vehicle.setAbs(new ABS());
    }

    @Override
    public void buildChassis() {
        vehicle.setChassis(new TruckChassis());
    }

    @Override
    public void buildClimateControlSystem() {
        vehicle.setClimateControlSystem(new ClimateControlSystem());
    }

    @Override
    public void buildAirConditioning() {
        vehicle.setAirConditioning(new AirConditioning());
    }

    @Override
    public void buildSeatHeating() {
        vehicle.setSeatHeating(new SeatHeating());
    }

    @Override
    public void buildEngine() {
        vehicle.setEngine(new TruckEngine());
    }

    @Override
    public void buildSteering() {
        vehicle.setSteering(new PowerSteering());
    }

    @Override
    public void buildWheels() {
        vehicle.setWheels(new TruckWheels());
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }
}
