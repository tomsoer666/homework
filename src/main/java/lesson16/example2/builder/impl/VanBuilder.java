package lesson16.example2.builder.impl;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.models.brakes.ABS;
import lesson16.example2.models.chassis.impl.VanChassis;
import lesson16.example2.models.climate.AirConditioning;
import lesson16.example2.models.climate.ClimateControlSystem;
import lesson16.example2.models.climate.SeatHeating;
import lesson16.example2.models.engine.impl.VanEngine;
import lesson16.example2.models.steering.impl.StandardSteering;
import lesson16.example2.models.vehicle.Van;
import lesson16.example2.models.vehicle.Vehicle;
import lesson16.example2.models.wheels.impl.VanWheels;

public class VanBuilder implements VehicleBuilder {
    private final Vehicle vehicle = new Van();

    @Override
    public void buildAbs() {
        vehicle.setAbs(new ABS());
    }

    @Override
    public void buildChassis() {
        vehicle.setChassis(new VanChassis());
    }

    @Override
    public void buildClimateControlSystem() {
        vehicle.setClimateControlSystem(new ClimateControlSystem());
    }

    @Override
    public void buildAirConditioning() {
        vehicle.setAirConditioning(new AirConditioning());
    }

    @Override
    public void buildSeatHeating() {
        vehicle.setSeatHeating(new SeatHeating());
    }

    @Override
    public void buildEngine() {
        vehicle.setEngine(new VanEngine());
    }

    @Override
    public void buildSteering() {
        vehicle.setSteering(new StandardSteering());
    }

    @Override
    public void buildWheels() {
        vehicle.setWheels(new VanWheels());
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }
}
