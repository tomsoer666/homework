package lesson16.example2.builder;

import lesson16.example2.models.vehicle.Vehicle;

public interface VehicleBuilder {
    void buildAbs();

    void buildChassis();

    void buildClimateControlSystem();

    void buildAirConditioning();

    void buildSeatHeating();

    void buildEngine();

    void buildSteering();

    void buildWheels();

    Vehicle getVehicle();
}
