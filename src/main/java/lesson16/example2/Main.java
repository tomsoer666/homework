package com.sbrf.lessons.lesson18.example2;

import lesson16.example2.builder.VehicleBuilder;
import lesson16.example2.builder.impl.CarBuilder;
import lesson16.example2.builder.impl.TruckBuilder;
import lesson16.example2.builder.impl.VanBuilder;
import lesson16.example2.director.VehicleDirector;
import lesson16.example2.director.impl.LuxeDirector;
import lesson16.example2.director.impl.StandardDirector;
import lesson16.example2.models.vehicle.Vehicle;

public class Main {

    public static void main(String[] args) {
        VehicleDirector standardDirector = new StandardDirector();
        VehicleDirector luxeDirector = new LuxeDirector();
        VehicleBuilder carBuilder = new CarBuilder();
        VehicleBuilder vanBuilder = new VanBuilder();
        VehicleBuilder truckBuilder = new TruckBuilder();

        Vehicle minimumEquipmentCar = standardDirector.build(carBuilder);
        Vehicle maximumEquipmentCar = luxeDirector.build(carBuilder);

        Vehicle minimumEquipmentVan = standardDirector.build(vanBuilder);
        Vehicle maximumEquipmentVan = luxeDirector.build(vanBuilder);

        Vehicle minimumEquipmentTruck = standardDirector.build(truckBuilder);
        Vehicle maximumEquipmentTruck = luxeDirector.build(truckBuilder);

        // some action with vehicle
        System.out.println(minimumEquipmentCar);
        System.out.println(maximumEquipmentCar);
        System.out.println(minimumEquipmentVan);
        System.out.println(maximumEquipmentVan);
        System.out.println(minimumEquipmentTruck);
        System.out.println(maximumEquipmentTruck);
    }
}
