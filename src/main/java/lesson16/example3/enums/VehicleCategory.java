package lesson16.example3.enums;

public enum VehicleCategory {
    AIRCRAFT,
    TRUCK,
    SHIP
}
