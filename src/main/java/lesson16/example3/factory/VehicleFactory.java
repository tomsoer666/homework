package lesson16.example3.factory;

import lesson16.example3.enums.VehicleCategory;
import lesson16.example3.exceptions.InvalidVehicleCategoryException;
import lesson16.example3.models.vehicle.Aircraft;
import lesson16.example3.models.vehicle.Ship;
import lesson16.example3.models.vehicle.Truck;
import lesson16.example3.models.vehicle.Vehicle;

public class VehicleFactory {
    public Vehicle createVehicle(VehicleCategory category) {
        switch (category) {
            case SHIP:
                return new Ship();
            case TRUCK:
                return new Truck();
            case AIRCRAFT:
                return new Aircraft();
            default:
                throw new InvalidVehicleCategoryException(category.toString() + " не существует");
        }
    }
}
