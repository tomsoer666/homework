package lesson16.example3;

import lesson16.example3.enums.VehicleCategory;
import lesson16.example3.factory.VehicleFactory;
import lesson16.example3.models.point.Point;
import lesson16.example3.models.route.Route;
import lesson16.example3.models.vehicle.Vehicle;
import lesson16.example3.services.LogisticSystem;

public class Main {

    public static void main(String[] args) {
        LogisticSystem logisticSystem = new LogisticSystem();

        Point departure = new Point("Moscow");
        Point destination = new Point("Saint-Petersburg");

        VehicleFactory vehicleFactory = new VehicleFactory();
        Vehicle truck = vehicleFactory.createVehicle(VehicleCategory.TRUCK);
        Vehicle aircraft = vehicleFactory.createVehicle(VehicleCategory.AIRCRAFT);
        Vehicle ship = vehicleFactory.createVehicle(VehicleCategory.SHIP);

        Route routeTruck = logisticSystem.createRoute(truck, departure, destination);
        Route routeAircraft = logisticSystem.createRoute(aircraft, departure, destination);
        Route routeShip = logisticSystem.createRoute(ship, departure, destination);

        truck.setRoute(routeTruck);
        truck.go();

        aircraft.setRoute(routeAircraft);
        aircraft.go();

        ship.setRoute(routeShip);
        ship.go();
    }
}

