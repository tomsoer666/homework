package lesson16.example3.models.vehicle;

import lesson16.example3.models.route.Route;

public abstract class Vehicle {
    private Route route;

    public void go() {
        // some implementation
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
