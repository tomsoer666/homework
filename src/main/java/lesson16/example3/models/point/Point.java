package lesson16.example3.models.point;

public class Point {
    private final String name;

    public Point(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
