package lesson16.example3.models.route;

import lesson16.example3.models.point.Point;

import java.util.List;

public class Route {

    private final List<Point> points;

    public Route(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }
}
