package lesson16.example3.exceptions;

public class InvalidVehicleCategoryException extends RuntimeException {
    public InvalidVehicleCategoryException(String message) {
        super(message);
    }
}
