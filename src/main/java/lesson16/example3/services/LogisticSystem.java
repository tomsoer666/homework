package lesson16.example3.services;

import lesson16.example3.models.point.Point;
import lesson16.example3.models.route.Route;
import lesson16.example3.models.vehicle.Aircraft;
import lesson16.example3.models.vehicle.Truck;
import lesson16.example3.models.vehicle.Vehicle;

import java.util.ArrayList;

public class LogisticSystem {

    public Route createRoute(Vehicle vehicle, Point departure, Point destination) {
        // some logic
        if (vehicle instanceof Aircraft) {
            return new Route(new ArrayList<>());
        } else if (vehicle instanceof Truck) {
            return new Route(new ArrayList<>());
        } else return new Route(new ArrayList<>());
    }
}
