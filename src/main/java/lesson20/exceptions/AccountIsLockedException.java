package lesson20.exceptions;

/*
 * Исключение вызывает при попытке ввода пин кода, после того
 * как пользователь неправильно подрят набрал пин код.
 * Для избежания исключения необходимо подождать разблокировку карты.
 * Время ожидания 5 секунд.
 * */
public class AccountIsLockedException extends RuntimeException {
    public AccountIsLockedException(String message, long time) {
        super(message + time);
    }
}
