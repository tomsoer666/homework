package lesson20.exceptions;

/*
 * Исключение вызывается при повторном вводе пин кода,
 * после успешного его ввода. Для избежания необходимо
 * вытащить карту из терминала, и повторно вставить карту
 * и ввести пин код.
 * */
public class CardAlreadyValidException extends RuntimeException {
    public CardAlreadyValidException(String message) {
        super(message);
    }
}
