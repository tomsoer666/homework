package lesson20.exceptions;

/*
 * Исключение вызывает при повторной попытке внести карту в Терминалю
 * Для избежания необходимо проверить наличие карты в терминале
 * и вытащить в случае необходимости работы с другой картой.
 * */
public class CardAlreadyInTerminalException extends RuntimeException {
    public CardAlreadyInTerminalException(String message) {
        super(message);
    }
}
