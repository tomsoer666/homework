package lesson20.exceptions;

/*
 * Исключение вызывает при попытке вызвать опертацию в терминале,
 * при отсутсвия прав на использования карты. Для избежания необходимо
 * ввести правильный пин код.
 * */
public class NoAccessRightException extends RuntimeException {
    public NoAccessRightException(String message) {
        super(message);
    }
}
