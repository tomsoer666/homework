package lesson20.models;

import lesson20.enums.AccountStatus;

public class AccountLock {
    private int countTry = 0;
    private AccountStatus accountStatus = AccountStatus.UNLOCKED;
    private long startTaskTime;

    public AccountLock() {
    }

    public int getCountTry() {
        return countTry;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public long getStartTaskTime() {
        return startTaskTime;
    }

    public void setStartTaskTime(long startTaskTime) {
        this.startTaskTime = startTaskTime;
    }

    public void incrementTry() {
        countTry++;
    }

    public void accessTry() {
        countTry = 0;
    }

    @Override
    public String toString() {
        return "AccountLock{" +
                "countTry=" + countTry +
                ", accountStatus=" + accountStatus +
                ", startTaskTime=" + startTaskTime +
                '}';
    }
}
