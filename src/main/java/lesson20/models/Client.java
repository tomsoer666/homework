package lesson20.models;

public class Client {
    private final String clientFirstName;
    private final String clientSecondName;
    private final String clientPatronymic;

    public Client(String clientFirstName, String clientSecondName, String clientPatronymic) {
        this.clientFirstName = clientFirstName;
        this.clientSecondName = clientSecondName;
        this.clientPatronymic = clientPatronymic;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public String getClientSecondName() {
        return clientSecondName;
    }

    public String getClientPatronymic() {
        return clientPatronymic;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientFirstName='" + clientFirstName + '\'' +
                ", clientSecondName='" + clientSecondName + '\'' +
                ", clientPatronymic='" + clientPatronymic + '\'' +
                '}';
    }
}
