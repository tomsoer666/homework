package lesson20.models;

public class RequestData<T> {
    private Long id;
    private T object;

    public Long getId() {
        return id;
    }

    public T getObject() {
        return object;
    }
}
