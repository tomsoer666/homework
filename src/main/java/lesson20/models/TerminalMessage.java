package lesson20.models;

public class TerminalMessage {
    public static final String CLIENT_NOT_EXIST = "Client not exist";
    public static final String ACCESS_DENIED = "No access right";
    public static final String ILLEGAL_CASH = "Illegal cash";
    public static final String NOT_ENOUGH_CASH = "Not enough money";
    public static final String ACCOUNT_LOCKED = "Account is locked." +
            " To unblock: ms = ";
    public static final String INVALID_PIN_CODE = "Invalid pin code";
    public static final String CARD_ALREADY_IN_TERMINAL = "Card already in terminal";
    public static final String EMPTY_TERMINAL = "Terminal is empty";
    public static final String CARD_VALID = "Pin code already entered";

    private TerminalMessage() {
        throw new UnsupportedOperationException("Can not instantiate utility class");
    }
}
