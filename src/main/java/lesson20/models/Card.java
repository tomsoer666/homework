package lesson20.models;

import lesson20.enums.CardStatus;

public class Card {
    private final Client client;
    private final int pinCode;
    private final AccountLock accountLock = new AccountLock();
    private Long clientCash;
    private boolean inTerminal = false;
    private CardStatus cardStatus = CardStatus.DECLINE;

    public Card(Client client, int pinCode, Long clientCash) {
        this.client = client;
        this.pinCode = pinCode;
        this.clientCash = clientCash;
    }

    public Client getClient() {
        return client;
    }

    public int getPinCode() {
        return pinCode;
    }

    public AccountLock getAccountLock() {
        return accountLock;
    }

    public Long getClientCash() {
        return clientCash;
    }

    public void setClientCash(Long clientCash) {
        this.clientCash = clientCash;
    }

    public boolean isInTerminal() {
        return inTerminal;
    }

    public void setInTerminal(boolean inTerminal) {
        this.inTerminal = inTerminal;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    @Override
    public String toString() {
        return "Card{" +
                "client=" + client +
                ", pinCode=" + pinCode +
                ", accountLock=" + accountLock +
                ", clientCash=" + clientCash +
                ", inTerminal=" + inTerminal +
                ", cardStatus=" + cardStatus +
                '}';
    }

}
