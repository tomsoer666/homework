package lesson20.logger;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.*;

@Aspect
public class Logging {

    private static final Logger log = Logger.getLogger(Logging.class);
    private static final String PATH_ROOT = "execution(public * lesson20.service.Terminal.* (..))";

    @Before(PATH_ROOT)
    public void beforeAdvice() {
        log.info("Initiating service.");
    }

    @After(PATH_ROOT)
    public void afterAdvice() {
        log.info("Service was initiated.");
    }

    @AfterReturning(pointcut = PATH_ROOT, returning = "someValue")
    public void afterReturningAdvice(Object someValue) {
        if (someValue == null) {
            log.info("Method was completed successful");
        } else
            log.info("Method return vale = : " + someValue.toString());
    }

    @AfterThrowing(pointcut = PATH_ROOT, throwing = "e")
    public void inCaseOfExceptionThrowAdvice(ClassCastException e) {
        log.info("We have an exception here: " + e.toString());
    }
}
