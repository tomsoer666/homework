package lesson20.controllers;

import lesson20.models.Card;
import lesson20.models.RequestData;
import lesson20.service.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TerminalController {
    private final Terminal terminal;

    @Autowired
    public TerminalController(Terminal terminal) {
        this.terminal = terminal;
    }

    @PostMapping(value = "/insertCard")
    public ResponseEntity<String> insertCard(@RequestBody RequestData<Card> session) {
        terminal.insertCard(session.getId(), session.getObject());
        return new ResponseEntity<>("Card inserted", HttpStatus.OK);
    }

    @PostMapping(value = "/enterPinCode")
    public ResponseEntity<String> enterPinCode(
            @RequestBody RequestData<Integer> session) {
        terminal.enterPinCode(session.getId(), session.getObject());
        return new ResponseEntity<>("Pin code correct", HttpStatus.OK);
    }

    @GetMapping(value = "/getCard/{data}")
    public String getCardList(@PathVariable(name = "data") RequestData<?> data) {
        return terminal.cardInfo(data.getId());
    }

    @GetMapping(value = "/pullOutCard/{data}")
    public ResponseEntity<String> pullOutCard(@PathVariable(name = "data") RequestData<?> data) {
        terminal.pullOutCard(data.getId());
        return new ResponseEntity<>("Card is out", HttpStatus.OK);
    }

    @PostMapping(value = "/putCash")
    public ResponseEntity<String> putCash(
            @RequestBody RequestData<Long> session) {
        terminal.putCash(session.getId(), session.getObject());
        return new ResponseEntity<>("Cash is putted", HttpStatus.OK);
    }

    @PostMapping(value = "/withdrawCash")
    public ResponseEntity<String> withdrawCash(
            @RequestBody RequestData<Long> session) {
        terminal.withdrawCash(session.getId(), session.getObject());
        return new ResponseEntity<>("Cash is withdraw", HttpStatus.OK);
    }

    @GetMapping(value = "/terminal/{data}")
    public ResponseEntity<String> isCardInTerminal(@PathVariable(name = "data") RequestData<?> data) {
        boolean isCardInTerminal = terminal.isCardInTerminal(data.getId());
        return new ResponseEntity<>("Card in terminal = " + isCardInTerminal,
                HttpStatus.OK);
    }
}
