package lesson20.service.impl;

import lesson20.enums.CardStatus;
import lesson20.exceptions.CardAlreadyInTerminalException;
import lesson20.exceptions.CardAlreadyValidException;
import lesson20.exceptions.CardNotInTerminalException;
import lesson20.exceptions.NoAccessRightException;
import lesson20.models.Card;
import lesson20.service.PinValidator;
import lesson20.service.Terminal;
import lesson20.service.TerminalServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static lesson20.models.TerminalMessage.*;

@Service
public class TerminalImpl implements Terminal {
    private final TerminalServer server;
    private final PinValidator pinValidator;
    private final Map<Long, Card> dataBase = new HashMap<>();

    @Autowired
    public TerminalImpl(final TerminalServer server,
                        final PinValidator pinValidator) {
        this.server = server;
        this.pinValidator = pinValidator;
    }

    @Override
    public void insertCard(Long sessionId, Card card) {
        if (isCardInTerminal(sessionId)) {
            throw new CardAlreadyInTerminalException(CARD_ALREADY_IN_TERMINAL);
        }
        card = new Card(card.getClient(), card.getPinCode(), card.getClientCash());
        card.setInTerminal(true);
        dataBase.putIfAbsent(sessionId, card);
    }

    @Override
    public void enterPinCode(Long sessionId, Integer pinCode) {
        if (!isCardInTerminal(sessionId)) {
            throw new CardNotInTerminalException(EMPTY_TERMINAL);
        }
        synchronized (dataBase) {
            checkPinCodeValid(pinCode, sessionId);
        }
    }

    @Override
    public void pullOutCard(Long sessionId) {
        if (!isCardInTerminal(sessionId)) {
            throw new CardNotInTerminalException(EMPTY_TERMINAL);
        }
        dataBase.remove(sessionId);
    }

    @Override
    public void putCash(Long sessionId, Long cash) {
        checkTerminal(sessionId);
        synchronized (dataBase) {
            Card card = checkCardOnNullOrReturnValue(sessionId);
            server.sendCash(card, cash);
        }
    }


    @Override
    public void withdrawCash(Long sessionId, Long cash) {
        checkTerminal(sessionId);
        synchronized (dataBase) {
            Card card = checkCardOnNullOrReturnValue(sessionId);
            server.takeCash(card, cash);
        }
    }


    @Override
    public String cardInfo(Long sessionId) {
        return server.getCardInfo(checkCardOnNullOrReturnValue(sessionId));
    }

    @Override
    public boolean isCardInTerminal(Long sessionId) {
        Card card = dataBase.get(sessionId);
        if (card != null) {
            return card.isInTerminal();
        }
        return false;
    }


    private void checkPinCodeValid(Integer pinCode, Long sessionId) {
        Card card = dataBase.get(sessionId);
        if (card.getCardStatus().equals(CardStatus.ACCESS)) {
            throw new CardAlreadyValidException(CARD_VALID);
        }
        pinValidator.isPinCodeValid(pinCode, card);
    }

    private void checkTerminal(Long sessionId) {
        Card card = dataBase.get(sessionId);
        if (!card.isInTerminal()) {
            throw new CardNotInTerminalException(EMPTY_TERMINAL);
        }
        if (card.getCardStatus().equals(CardStatus.DECLINE)) {
            throw new NoAccessRightException(ACCESS_DENIED);
        }
    }

    private Card checkCardOnNullOrReturnValue(Long sessionId) {
        Card card = dataBase.get(sessionId);
        if (card == null) {
            throw new CardNotInTerminalException(EMPTY_TERMINAL);
        }
        return card;
    }
}
