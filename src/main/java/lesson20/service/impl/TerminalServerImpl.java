package lesson20.service.impl;

import lesson20.enums.CardStatus;
import lesson20.exceptions.ClientNotExistsException;
import lesson20.exceptions.IllegalCashException;
import lesson20.exceptions.NoAccessRightException;
import lesson20.exceptions.NotEnoughCashException;
import lesson20.models.Card;
import lesson20.service.TerminalServer;
import org.springframework.stereotype.Service;

import static lesson20.models.TerminalMessage.*;

@Service
public class TerminalServerImpl implements TerminalServer {
    @Override
    public void sendCash(Card card, long cash) {
        checkCardAndCash(card, cash);
        card.setClientCash(card.getClientCash() + cash);
    }

    @Override
    public void takeCash(Card card, long cash) {
        checkCardAndCash(card, cash);
        if (card.getClientCash() < cash) {
            throw new NotEnoughCashException(NOT_ENOUGH_CASH);
        }
        card.setClientCash(card.getClientCash() - cash);
    }

    @Override
    public String getCardInfo(Card card) {
        checkCard(card);
        return card.toString();
    }

    private void checkCardAndCash(Card card, long cash) {
        checkCard(card);
        if (cash % 100L != 0L || cash < 0L) {
            throw new IllegalCashException(ILLEGAL_CASH);
        }
    }

    private void checkCard(Card card) {
        if (card == null) {
            throw new ClientNotExistsException(CLIENT_NOT_EXIST);
        }
        if (card.getCardStatus().equals(CardStatus.DECLINE)) {
            throw new NoAccessRightException(ACCESS_DENIED);
        }
    }
}
