package lesson20.service.impl;

import lesson20.enums.AccountStatus;
import lesson20.enums.CardStatus;
import lesson20.exceptions.AccountIsLockedException;
import lesson20.exceptions.ClientNotExistsException;
import lesson20.exceptions.IncorrectPinException;
import lesson20.models.Card;
import lesson20.service.PinValidator;
import lesson20.service.TimerLock;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;

import static lesson20.models.TerminalMessage.*;

@Service
public class PinValidatorImpl implements PinValidator {
    private static final long DELAY = 5000L;

    @Override
    public boolean isPinCodeValid(Integer pinCode, Card card) {
        checkLockStatus(card);
        if (pinCode != card.getPinCode()) {
            card.getAccountLock().incrementTry();
            if (card.getAccountLock().getCountTry() >= 3) {
                lockAccount(card);
            }
            throw new IncorrectPinException(INVALID_PIN_CODE);
        }
        return access(card);
    }

    private boolean access(Card card) {
        card.getAccountLock().accessTry();
        card.setCardStatus(CardStatus.ACCESS);
        return true;
    }

    private void lockAccount(Card card) {
        TimerTask timerLock = new TimerLock(card);
        card.getAccountLock().setAccountStatus(AccountStatus.LOCKED);
        card.getAccountLock().accessTry();
        new Timer().schedule(timerLock, DELAY);
        card.getAccountLock().setStartTaskTime(System.currentTimeMillis() + DELAY);
    }

    private void checkLockStatus(Card card) {
        if (card == null) {
            throw new ClientNotExistsException(CLIENT_NOT_EXIST);
        }
        if (card.getAccountLock().getAccountStatus() == AccountStatus.LOCKED) {
            throw new AccountIsLockedException(ACCOUNT_LOCKED
                    , card.getAccountLock().getStartTaskTime() - System.currentTimeMillis());
        }
    }
}
