package lesson20.service;

import lesson20.enums.AccountStatus;
import lesson20.models.Card;

import java.util.TimerTask;

public class TimerLock extends TimerTask {
    private Card card;

    public TimerLock(Card card) {
        this.card = card;
    }

    @Override
    public void run() {
        card.getAccountLock().setAccountStatus(AccountStatus.UNLOCKED);
    }
}
