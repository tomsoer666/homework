package lesson20.service;

import lesson20.models.Card;

public interface TerminalServer {
    void sendCash(Card card, long cash);

    void takeCash(Card card, long cash);

    String getCardInfo(Card card);
}
