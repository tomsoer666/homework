package lesson20.service;

import lesson20.models.Card;

public interface Terminal {
    void insertCard(Long sessionId, Card card);

    void enterPinCode(Long sessionId, Integer pinCode);

    void pullOutCard(Long sessionId);

    void putCash(Long sessionId, Long cash);

    void withdrawCash(Long sessionId, Long cash);

    String cardInfo(Long sessionId);

    boolean isCardInTerminal(Long sessionId);
}
