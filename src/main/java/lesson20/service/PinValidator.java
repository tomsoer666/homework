package lesson20.service;

import lesson20.models.Card;

public interface PinValidator {
    boolean isPinCodeValid(Integer pinCode, Card card);
}
