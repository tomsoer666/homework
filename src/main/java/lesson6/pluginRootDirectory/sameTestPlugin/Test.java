package lesson6.pluginRootDirectory.sameTestPlugin;

import lesson6.interfaces.Plugin;

public class Test implements Plugin {
    public Test() {
    }

    @Override
    public void doUseful() {
        System.out.println("Hello same test!");
    }
}
