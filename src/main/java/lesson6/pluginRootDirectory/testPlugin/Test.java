package lesson6.pluginRootDirectory.testPlugin;

import lesson6.interfaces.Plugin;

public class Test implements Plugin {
    public Test() {

    }

    @Override
    public void doUseful() {
        System.out.println("Hello test!");
    }
}
