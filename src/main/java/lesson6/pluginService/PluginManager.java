package lesson6.pluginService;

import lesson6.interfaces.Plugin;

import java.io.IOException;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws ClassNotFoundException {
        PluginClassLoader pluginClassLoader = new PluginClassLoader();
        try {
            StringBuilder pluginPath = new StringBuilder();
            pluginPath.append(pluginRootDirectory).append(pluginName).append(pluginClassName);
            Class<?> c = pluginClassLoader.findClass(pluginPath.toString());
            return (Plugin) c.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        throw new ClassNotFoundException();
    }
}
