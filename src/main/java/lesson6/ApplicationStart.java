package lesson6;

import lesson6.interfaces.Plugin;
import lesson6.pluginService.PluginManager;

import java.io.IOException;

public class ApplicationStart {
    public static void main(String[] args) throws ClassNotFoundException {
        PluginManager pluginManager =
                new PluginManager("lesson6.pluginRootDirectory.");
        try {
            Plugin test = pluginManager.load("testPlugin.",
                    "Test");
            Plugin sameTest = pluginManager.load("sameTestPlugin.",
                    "Test");
            Plugin string = pluginManager.load("pluginString.",
                    "String");
            test.doUseful();
            sameTest.doUseful();
            string.doUseful();
            System.out.println(test.getClass());
            System.out.println(sameTest.getClass());
            System.out.println(string.getClass());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
