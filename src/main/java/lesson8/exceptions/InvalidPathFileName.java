package lesson8.exceptions;
/*
    Исключение возникает при неверном указаном пути к файлу.
    Необходимо проверить правильность ввода пути к файлу.
 */
public class InvalidPathFileName extends RuntimeException{
    public InvalidPathFileName(String message) {
        super(message);
    }
}
