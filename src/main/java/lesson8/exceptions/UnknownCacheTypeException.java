package lesson8.exceptions;
/*
      Исключение вызывается если было модифицировано значение cacheType в Cache. Для избежания
      исключения необходимо работать только с имеющимися типами CacheType
 */
public class UnknownCacheTypeException extends RuntimeException {
    public UnknownCacheTypeException(String message) {
        super(message);
    }
}
