package lesson8.exceptions;
/*
    Исключение вызывается, если при десириализации классы не будут совпадать.
    Так же возможное возникновение, если при сериализации возникает непредвиденное
    ошибка в работе, прекращение работы. Для избежания ошибка нельзя вручную менять
    структуру класса в Кеш файле, если ошибка JVM, то необходимо удалить Кеш файл
 */
public class CorruptedClassException extends RuntimeException {
    public CorruptedClassException(String message) {
        super(message);
    }
}
