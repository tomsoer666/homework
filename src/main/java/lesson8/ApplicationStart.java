package lesson8;

import lesson8.dynamicProxy.cache.CacheProxy;
import lesson8.interfaces.Service;
import lesson8.interfaces.impl.ServiceImpl;

public class ApplicationStart {
    public static void main(String[] args) {
        CacheProxy cacheProxy = new CacheProxy();
        //CacheProxy cacheProxy = new CacheProxy("c://java//");
        Service service = (Service) cacheProxy.cache(new ServiceImpl());
        System.out.println(service.run("Ilya", 1L));
        System.out.println(service.run("Ilya", 0L));
        System.out.println(service.run("Igor", 0L));
        System.out.println();
        System.out.println(service.doHardWork("Vladimir", 301L));
        System.out.println(service.doHardWork("Ilya", 301L));
        System.out.println(service.doHardWork("Peter", 304L));
        System.out.println(service.doHardWork("Ilya", 301L));
        System.out.println(service.doHardWork("Vladimir", 303L));

    }
}
