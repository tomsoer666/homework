package lesson8.dynamicProxy;

import lesson8.dynamicProxy.cache.validator.Validator;
import lesson8.models.CacheData;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

public class CacheHandler implements InvocationHandler {
    private final Object delegate;
    private final String pathFile;
    private List<CacheData> cacheData;
    private final Validator validator = new Validator();

    public CacheHandler(Object delegate, List<CacheData> cacheData, String pathFile) {
        this.delegate = delegate;
        this.cacheData = cacheData;
        this.pathFile = pathFile;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return validator.valid(method, delegate, args, cacheData, pathFile);
    }
}
