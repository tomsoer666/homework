package lesson8.dynamicProxy.cache.enums;

public enum CacheType {
    FILE,
    IN_MEMORY
}
