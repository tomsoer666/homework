package lesson8.dynamicProxy.cache;

import lesson8.dynamicProxy.CacheHandler;
import lesson8.interfaces.Service;
import lesson8.models.CacheData;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

public class CacheProxy {
    private String pathFile = "";

    public CacheProxy() {
    }

    public CacheProxy(String pathFile) {
        this.pathFile = pathFile;
    }

    public Object cache(Object obj) {
        List<CacheData> cacheData = new ArrayList<>();
        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                new Class[]{Service.class}, new CacheHandler(obj, cacheData, pathFile));
    }
}
