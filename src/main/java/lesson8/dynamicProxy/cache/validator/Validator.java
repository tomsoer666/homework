package lesson8.dynamicProxy.cache.validator;

import lesson8.dynamicProxy.cache.annotations.Cache;
import lesson8.exceptions.CorruptedClassException;
import lesson8.exceptions.InvalidPathFileName;
import lesson8.exceptions.UnknownCacheTypeException;
import lesson8.models.CacheData;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static java.util.stream.Collectors.toList;

public class Validator {
    public Object valid(Method method, Object delegate, Object[] args,
                        List<CacheData> cacheData, String pathFile)
            throws IOException, InvocationTargetException, IllegalAccessException {
        if (method.isAnnotationPresent(Cache.class)) {
            Cache cache = method.getAnnotation(Cache.class);
            switch (cache.cacheType()) {
                case IN_MEMORY: {
                    if (cacheData.isEmpty()) {
                        return validateNotExistenceMemoryCache(cacheData, cache,
                                method, delegate, args);
                    } else {
                        return validateExistenceMemoryCache(cacheData, cache,
                                method, delegate, args);
                    }
                }
                case FILE: {
                    if (cache.fileNamePrefix().isEmpty()) {
                        if (new File(pathFile + method.getName()).exists()) {
                            return validateExistenceCache(cache, pathFile + method.getName(),
                                    method, delegate, args);
                        } else
                            return validateNotExistenceCache(cache, pathFile + method.getName(),
                                    method, delegate, args);
                    } else {
                        if (new File(pathFile + cache.fileNamePrefix()).exists()) {
                            return validateExistenceCache(cache, pathFile + cache.fileNamePrefix(),
                                    method, delegate, args);
                        } else
                            return validateNotExistenceCache(cache, pathFile + cache.fileNamePrefix(),
                                    method, delegate, args);
                    }
                }
                default:
                    throw new UnknownCacheTypeException("Не существующий тип кеширования");
            }
        }
        return method.invoke(delegate, args);
    }

    private Object validateExistenceMemoryCache(List<CacheData> cacheData, Cache cache,
                                                Method method, Object delegate, Object[] args) throws InvocationTargetException, IllegalAccessException {
        CacheData resultData = getIdentifiedResultData(cache, args, cacheData);
        if (resultData == null) {
            System.out.println("Кешируем данные в JVM");
            Object result = method.invoke(delegate, args);
            if (result instanceof List) {
                result = validateListData((List<Object>) result, cache.listSize());
            }
            CacheData data = new CacheData((String) args[0], (Long) args[1], result);
            cacheData.add(data);
            return result;
        } else {
            System.out.println("Чтение из кеша JVM");
            return resultData.getResult();
        }
    }

    private Object validateNotExistenceMemoryCache(List<CacheData> cacheData, Cache cache,
                                                   Method method, Object delegate, Object[] args)
            throws InvocationTargetException, IllegalAccessException {
        System.out.println("Кешируем данные в JVM");
        Object result = method.invoke(delegate, args);
        if (result instanceof List) {
            result = validateListData((List<Object>) result, cache.listSize());
        }
        cacheData.add((new CacheData((String) args[0], (Long) args[1], result)));
        return result;
    }

    private Object validateExistenceCache(Cache cache, String fileName, Method method,
                                          Object delegate, Object[] args)
            throws IOException, InvocationTargetException, IllegalAccessException {
        List<CacheData> data = getCacheData(fileName);
        CacheData resultData = getIdentifiedResultData(cache, args, data);
        if (resultData == null) {
            System.out.println("Кешируем данные");
            Object result = method.invoke(delegate, args);
            if (result instanceof List) {
                result = validateListData((List<Object>) result, cache.listSize());
            }
            CacheData cacheData = new CacheData((String) args[0], (Long) args[1], result);
            data.add(cacheData);
            writeDataInCache(fileName, data);
            if (cache.zip()) {
                cacheToZip(fileName);
            }
            return result;
        } else {
            System.out.println("Чтение из кеша");
            if (cache.zip()) {
                cacheToZip(fileName);
            }
            return resultData.getResult();
        }
    }

    private CacheData getIdentifiedResultData(Cache cache,
                                              Object[] args, List<CacheData> data) {
        Class<?>[] classes = cache.identifyBy();
        Set<Object> identifiedArgs = new HashSet<>();
        for (int i = 0; i < classes.length; i++) {
            for (Object arg : args) {
                if (arg.getClass().equals(classes[i])) {
                    identifiedArgs.add(arg);
                }
            }
        }
        if (identifiedArgs.size() == 1) {
            CacheData resultData = null;
            for (Object arg : identifiedArgs) {
                resultData = data.stream()
                        .filter(cacheData ->
                                (cacheData.getName().equals(arg)) ||
                                        (cacheData.getValue().equals(arg))
                        )
                        .findFirst()
                        .orElse(null);
            }
            return resultData;
        } else return getResultData(args, data);
    }

    private CacheData getResultData(Object[] args, List<CacheData> data) {
        return data.stream()
                .filter(cacheData ->
                        (cacheData.getName().equals((String) args[0])) &&
                                (cacheData.getValue().equals((Long) args[1]))
                )
                .findFirst()
                .orElse(null);
    }

    private void writeDataInCache(String fileName, List<CacheData> data) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(data);
        } catch (FileNotFoundException e) {
            throw new InvalidPathFileName("Не верно указана директория файла");
        }
    }

    private List<CacheData> getCacheData(String fileName) throws IOException {
        List<CacheData> data;
        try (FileInputStream fis = new FileInputStream(fileName)) {
            try (ObjectInputStream in = new ObjectInputStream(fis)) {
                data = (List<CacheData>) in.readObject();
            } catch (ClassNotFoundException e) {
                throw new CorruptedClassException("Ошибка чтения класса из файла");
            }
        }
        return data;
    }

    private Object validateNotExistenceCache(Cache cache, String fileName, Method method,
                                             Object delegate, Object[] args)
            throws IOException, InvocationTargetException, IllegalAccessException {
        System.out.println("Кешируем данные");
        Object result = method.invoke(delegate, args);
        if (result instanceof List) {
            result = validateListData((List<Object>) result, cache.listSize());
        }
        List<CacheData> cacheData = Stream.of(new CacheData((String) args[0], (Long) args[1], result))
                .collect(toList());
        writeDataInCache(fileName, cacheData);
        if (cache.zip()) {
            cacheToZip(fileName);
        }
        return result;
    }

    private void cacheToZip(String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream("cache.zip");
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        File fileToZip = new File(fileName);
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        zipOut.close();
        fis.close();
        fos.close();
    }

    private Object validateListData(List<Object> result, int listSize) {
        return result.stream().limit(listSize).collect(toList());
    }
}