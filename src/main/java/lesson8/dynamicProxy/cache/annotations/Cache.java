package lesson8.dynamicProxy.cache.annotations;

import lesson8.dynamicProxy.cache.enums.CacheType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cache {
    CacheType cacheType() default CacheType.FILE;

    String fileNamePrefix() default "";

    boolean zip() default false;

    Class<?>[] identifyBy() default {};

    int listSize() default Integer.MAX_VALUE;
}
