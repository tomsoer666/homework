package lesson8.models;

import java.io.Serializable;

public class CacheData implements Serializable {
    private String name;
    private Long value;
    private Object result;

    public CacheData() {
    }

    public CacheData(String name, Long value, Object result) {
        this.name = name;
        this.value = value;
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
