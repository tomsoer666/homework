package lesson8.interfaces.impl;

import lesson8.interfaces.Service;

import java.util.Arrays;
import java.util.List;

public class ServiceImpl implements Service {

    @Override
    public List<Long> run(String name, Long value) {
        return Arrays.asList(value + 1L, value + 3L, value + 4L, value + 5L, value + 6L);
    }

    @Override
    public Long doHardWork(String name, Long value) {
        return value + 1;
    }

}
