package lesson8.interfaces;

import lesson8.dynamicProxy.cache.annotations.Cache;
import lesson8.dynamicProxy.cache.enums.CacheType;

import java.util.List;

public interface Service {
    @Cache(cacheType = CacheType.IN_MEMORY, listSize = 3, identifyBy = {String.class})
    List<Long> run(String s, Long l);

    @Cache(cacheType = CacheType.FILE, fileNamePrefix = "cache_file", zip = true)
    Long doHardWork(String s, Long l);
}
