package lesson10.part2.interfaces.impl;

import lesson10.part2.interfaces.ThreadPool;

import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {
    private final int min;
    private final int max;
    private int size;
    private PoolThreads[] threads;
    private final LinkedBlockingQueue<Runnable> queue;

    public ScalableThreadPool(int min, int max) {
        if (min < max) {
            this.min = min;
            this.max = max;
        } else {
            this.min = max;
            this.max = min;
        }
        size = this.min;
        queue = new LinkedBlockingQueue<>();
        threads = new PoolThreads[size];
    }

    @Override
    public void start() {
        for (int i = 0; i < size; i++) {
            threads[i] = new PoolThreads();
            threads[i].start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (queue) {
            queue.add(runnable);
            checkBusy();
            queue.notify();
        }
    }

    private void checkBusy() {
        if (threads.length == min) {
            if (queue.size() > min) {
                threads = Arrays.copyOf(threads, max);
                for (int i = min; i < threads.length; i++) {
                    threads[i] = new PoolThreads();
                    threads[i].start();
                }
            }
        } else if (threads.length == max) {
            if (queue.isEmpty()) {
                for (int i = min; i < threads.length; i++) {
                    threads[i] = null;
                }
                threads = Arrays.copyOf(threads, min);
            }
        }
    }

    @Override
    public void shutdown() {
        System.out.println("Отлючение потоков из пула");
        for (int i = 0; i < size; i++) {
            threads[i] = null;
        }
    }

    private class PoolThreads extends Thread {
        @Override
        public void run() {
            Runnable task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Ошибка во время ожидания потока " + e.getMessage());
                        }
                    }
                    task = queue.poll();
                }
                try {
                    System.out.println(Thread.currentThread().getName() + " work");
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Пул потоков был непредвидено прерван " + e.getMessage());
                }
            }
        }
    }
}
