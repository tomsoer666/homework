package lesson10.part2.interfaces.impl;

import lesson10.part2.interfaces.ThreadPool;

import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {
    private final int size;
    private final PoolThreads[] threads;
    private final LinkedBlockingQueue<Runnable> queue;

    public FixedThreadPool(int size) {
        this.size = size;
        queue = new LinkedBlockingQueue<>();
        threads = new PoolThreads[size];
    }

    @Override
    public void start() {
        for (int i = 0; i < size; i++) {
            threads[i] = new PoolThreads();
            threads[i].start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (queue) {
            queue.add(runnable);
            queue.notify();
        }
    }

    @Override
    public void shutdown() {
        System.out.println("Отлючение потоков из пула");
        for (int i = 0; i < size; i++) {
            threads[i] = null;
        }
    }

    private class PoolThreads extends Thread {
        @Override
        public void run() {
            Runnable task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Ошибка во время ожидания потока " + e.getMessage());
                        }
                    }
                    task = queue.poll();
                }
                try {
                    System.out.println(Thread.currentThread().getName() + " work");
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Пул потоков был непредвидено прерван " + e.getMessage());
                }
            }
        }
    }
}
