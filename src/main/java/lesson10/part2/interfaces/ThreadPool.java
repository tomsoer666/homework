package lesson10.part2.interfaces;

public interface ThreadPool {
    void start();

    void execute(Runnable runnable);

    void shutdown();
}
