package lesson10.part2;

import lesson10.part2.interfaces.ThreadPool;
import lesson10.part2.interfaces.impl.FixedThreadPool;
import lesson10.part2.interfaces.impl.ScalableThreadPool;

public class ApplicationStart {
    public static void main(String[] args) throws InterruptedException {
        ThreadPool scaleThreadPool = new ScalableThreadPool(5, 7);
        ThreadPool fixedThreadPool = new FixedThreadPool(6);
        scaleThreadPool.start();
        fixedThreadPool.start();
        for (int i = 0; i < 20; i++) {
            Runnable runnable = () -> {
                try {
                    Thread.sleep(500);
                    System.out.println("Some action " + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            fixedThreadPool.execute(runnable);
            scaleThreadPool.execute(runnable);
        }
    }
}
