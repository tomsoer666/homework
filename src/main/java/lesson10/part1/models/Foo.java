package lesson10.part1.models;

import java.lang.reflect.Method;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Foo {
    private ReentrantLock reentrantLock;
    private Condition condition;
    private int iterator;

    public Foo() {
        reentrantLock = new ReentrantLock();
        condition = reentrantLock.newCondition();
        iterator = 0;
    }

    public void first() {
        reentrantLock.lock();
        try {
            while (iterator != 0) {
                condition.await();
            }
            System.out.println("first");
            iterator++;
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
    }

    public void second() {
        reentrantLock.lock();
        try {
            while (iterator != 1) {
                condition.await();
            }
            System.out.println("second");
            iterator++;
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
    }

    public void third() {
        reentrantLock.lock();
        try {
            while (iterator != 2) {
                condition.await();
            }
            System.out.println("third");
            iterator = 0;
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            reentrantLock.unlock();
        }
    }

    public void first(Runnable printFirst) throws InterruptedException {
        printFirst.run();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        printSecond.run();
    }

    public void third(Runnable printThird) throws InterruptedException {
        printThird.run();
    }
}

