package lesson10.part1.threads;

import lesson10.part1.models.Foo;

public class ThreadA implements Runnable {
    private Foo foo;

    public ThreadA(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.first();
    }
}
