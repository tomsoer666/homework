package lesson10.part1.threads;

import lesson10.part1.models.Foo;

public class ThreadC implements Runnable {
    private Foo foo;

    public ThreadC(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.third();
    }
}
