package lesson10.part1.threads;

import lesson10.part1.models.Foo;

public class ThreadB implements Runnable {
    private Foo foo;

    public ThreadB(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.second();
    }
}
