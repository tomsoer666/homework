package lesson10.part1;

import lesson10.part1.models.Foo;
import lesson10.part1.threads.ThreadA;
import lesson10.part1.threads.ThreadB;
import lesson10.part1.threads.ThreadC;

public class ApplicationStart {
    public static void main(String[] args) {
        Foo foo = new Foo();
        Thread threadA = new Thread(new ThreadA(foo));
        Thread threadB = new Thread(new ThreadB(foo));
        Thread threadC = new Thread(new ThreadC(foo));
        threadB.start();
        threadC.start();
        threadA.start();
    }
}
