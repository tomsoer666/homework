package lesson4.interfaces;

import lesson4.models.Card;

public interface Terminal {
    void insertCard(Card card);

    void enterPinCode(int pinCode, Card card);

    void pullOutCard();

    void putCash(long cash, Card card);

    void withdrawCash(long cash, Card card);

    String cardInfo(Card card);

    boolean isCardInTerminal();
}
