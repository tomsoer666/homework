package lesson4.service;

import lesson4.enums.AccountStatus;
import lesson4.models.Card;

import java.util.TimerTask;

public class TimerLock extends TimerTask {
    private Card card;

    public TimerLock(Card card) {
        this.card = card;
    }

    @Override
    public void run() {
        card.setAccountStatus(AccountStatus.UNLOCKED);
    }
}
