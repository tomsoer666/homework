package lesson4.service;

import lesson4.exceptions.ClientNotExistsException;
import lesson4.exceptions.IllegalCashException;
import lesson4.exceptions.NoAccessRightException;
import lesson4.exceptions.NotEnoughCashException;
import lesson4.models.Card;

public class TerminalServer {
    public void sendCash(boolean pinCodeValid, Card card, long cash) {
        if (card == null) {
            throw new ClientNotExistsException("Клиента не существует");
        }
        if (!pinCodeValid) {
            throw new NoAccessRightException("Нет прав доступа");
        }
        if (cash % 100L != 0L || cash < 0L) {
            throw new IllegalCashException("Некорректная сумма наличных");
        }
        card.setClientCash(card.getClientCash() + cash);
    }

    public void takeCash(boolean pinCodeValid, Card card, long cash) {
        if (card == null) {
            throw new ClientNotExistsException("Клиента не существует");
        }
        if (!pinCodeValid) {
            throw new NoAccessRightException("Нет прав доступа");
        }
        if (cash % 100L != 0L || cash < 0L) {
            throw new IllegalCashException("Некорректная сумма наличных");
        }
        if (card.getClientCash() < cash) {
            throw new NotEnoughCashException("Недостаточно денег на счете");
        }
        card.setClientCash(card.getClientCash() - cash);
    }

    public String getCardInfo(boolean pinCodeValid, Card card) {
        if (card == null) {
            throw new ClientNotExistsException("Клиента не существует");
        }
        if (!pinCodeValid) {
            throw new NoAccessRightException("Нет прав доступа");
        }
        return card.toString();
    }
}
