package lesson4.models;

import lesson4.enums.AccountStatus;
import lesson4.enums.CardStatus;

import java.util.Objects;

public class Card {
    private String clientFirstName;
    private String clientSecondName;
    private String clientPatronymic;
    private long clientCash;
    private int pinCode;
    private int countTry = 0;
    private AccountStatus accountStatus = AccountStatus.UNLOCKED;
    private CardStatus cardStatus = CardStatus.DECLINE;

    public Card(String clientFirstName, String clientSecondName, String clientPatronymic, long clientCash, int pinCode) {
        this.clientFirstName = clientFirstName;
        this.clientSecondName = clientSecondName;
        this.clientPatronymic = clientPatronymic;
        this.clientCash = clientCash;
        this.pinCode = pinCode;
    }

    public long getClientCash() {
        return clientCash;
    }

    public void setClientCash(long clientCash) {
        this.clientCash = clientCash;
    }

    public int getPinCode() {
        return pinCode;
    }

    public int getCountTry() {
        return countTry;
    }

    public void incrementTry() {
        countTry++;
    }

    public void accessTry() {
        countTry = 0;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return clientCash == card.clientCash &&
                pinCode == card.pinCode &&
                countTry == card.countTry &&
                clientFirstName.equals(card.clientFirstName) &&
                clientSecondName.equals(card.clientSecondName) &&
                clientPatronymic.equals(card.clientPatronymic) &&
                accountStatus == card.accountStatus &&
                cardStatus == card.cardStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientFirstName, clientSecondName, clientPatronymic, clientCash, pinCode, countTry, accountStatus, cardStatus);
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + clientFirstName + '\'' +
                ", secondName='" + clientSecondName + '\'' +
                ", patronymic='" + clientPatronymic + '\'' +
                ", cash=" + clientCash +
                '}';
    }
}
