package lesson4;

import lesson4.interfaces.impl.TerminalImpl;
import lesson4.models.Card;

import java.util.Scanner;

public class ApplicationStart {
    static final TerminalImpl TERMINAL = new TerminalImpl();
    static final String TERMINAL_INFO = "Команды для работы с треминалом: \n" +
            "insert - вставить карту в терминал; \n" +
            "pullout - вытащить карту из терминала; \n" +
            "pin code - повторно ввести пин код" +
            "put cash - положить на карту деньги; \n" +
            "withdraw cash - снять с карты деньги; \n" +
            "info - информация о карте; \n" +
            "card - проверка на наличии карты в терминале; \n" +
            "exit - выход работы из терминала; \n" +
            "help - информация о работе с терминалом. \n";
    static final String INSERT_INFO = "Вставьте карту. Для выбора карты введите: \n" +
            "card1 или card2. Для отмены операции введите cancel";
    static final String PIN_CODE_INFO = "Введите пин код";
    static final String VALID_PIN_CODE = "Пин код успешно введен. Выберите действия с картой";
    static final String CANCEL_OPERATION = "Отмена операции";
    static final String INVALID_OPERATION = "Неправильно введенна операция";
    static final Scanner scanner = new Scanner(System.in);
    static Card insertedCard = null;

    public static void main(String[] args) {

        Card card = new Card("Ilya", "Trukhov", "Igorevich",
                2000, 6666);
        Card card2 = new Card("Ilya", "Kozlov", "Vicktorovich",
                5000, 111);

        System.out.println(TERMINAL_INFO);
        while (true) {
            try {
                switch (scanner.nextLine()) {
                    case "insert": {
                        System.out.println(INSERT_INFO);
                        switch (scanner.nextLine()) {
                            case "card1": {
                                insertAlgorithm(card);
                                break;
                            }
                            case "card2": {
                                insertAlgorithm(card2);
                                break;
                            }
                            case "cancel": {
                                System.out.println(CANCEL_OPERATION);
                                break;
                            }
                            default: {
                                System.out.println(INVALID_OPERATION);
                                break;
                            }
                        }
                        break;
                    }
                    case "pullout": {
                        TERMINAL.pullOutCard();
                        System.out.println("Карта успешно вытащина");
                        insertedCard = null;
                        break;
                    }
                    case "pin code": {
                        System.out.println(PIN_CODE_INFO);
                        TERMINAL.enterPinCode(Integer.parseInt(scanner.nextLine()), insertedCard);
                        System.out.println(VALID_PIN_CODE);
                        break;
                    }
                    case "put cash": {
                        TERMINAL.cardInfo(insertedCard);
                        System.out.println("Введите сумму для внесения.");
                        TERMINAL.putCash(Long.parseLong(scanner.nextLine()), insertedCard);
                        break;
                    }
                    case "withdraw cash": {
                        TERMINAL.cardInfo(insertedCard);
                        System.out.println("Введите сумму для снятия");
                        TERMINAL.withdrawCash(Long.parseLong(scanner.nextLine()), insertedCard);
                        break;
                    }
                    case "info": {
                        System.out.println(TERMINAL.cardInfo(insertedCard));
                        break;
                    }
                    case "card": {
                        if (TERMINAL.isCardInTerminal()) {
                            System.out.println("Карта в терминале");
                        } else System.out.println("В терминале отсутсвует карта");
                    }
                    case "help": {
                        System.out.println(TERMINAL_INFO);
                        break;
                    }
                    case "exit": {
                        System.out.println("Работа завершена");
                        return;
                    }
                    default: {
                        System.out.println("Неверно введена команда, проверти список команд help");
                    }
                }
            } catch (NumberFormatException e) {
                System.err.println("Введите число");
            } catch (RuntimeException e) {
                System.err.println(e.getMessage());
            } finally {
                continue;
            }
        }
    }

    private static void insertAlgorithm(Card card) {
        TERMINAL.insertCard(card);
        insertedCard = card;
        System.out.println(PIN_CODE_INFO);
        TERMINAL.enterPinCode(Integer.parseInt(scanner.nextLine()), card);
        System.out.println(VALID_PIN_CODE);
    }
}
