package lesson4.exceptions;

/*
 * Исключение вызывает при попытке передать невалидное значения,
 * т.е отрицательное, не кратное 100 в сервис терминала. Для избежания необходимо
 * проверить передоваемое значение в сервис терминала для ввода или вывода денег.
 * */
public class IllegalCashException extends IllegalArgumentException {
    public IllegalCashException(String message) {
        super(message);
    }
}
