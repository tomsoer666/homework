package lesson4.exceptions;

/*
 * Исключение вызывает при попытке вызвать опертацию в терминале,
 * при недостатке средств на карте. Для избежания необходимо
 * проверить наличие средств на карте.
 * */
public class NotEnoughCashException extends RuntimeException {
    public NotEnoughCashException(String message) {
        super(message);
    }
}
