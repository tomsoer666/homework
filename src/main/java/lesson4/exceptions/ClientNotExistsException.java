package lesson4.exceptions;

/*
 * Исключение вызывает при попытке вызвать опертацию в терминале,
 * с несуществующей Null картой. Для избежания необходимо передавать
 * реальные NotNull данные в операции терминала.
 * */
public class ClientNotExistsException extends NullPointerException {
    public ClientNotExistsException(String message) {
        super(message);
    }
}
