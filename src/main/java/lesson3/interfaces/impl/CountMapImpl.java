package lesson3.interfaces.impl;

import lesson3.interfaces.CountMap;

import java.util.Map;
import java.util.TreeMap;

public class CountMapImpl<K> implements CountMap<K> {

    private static class Node<K> implements CountMap.Entry<K> {
        private int value = 0;
        private K key = null;
        private Node<K> next = null;


        @Override
        public K getKey() {
            return key;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public void setKey(K k) {
            this.key = k;
        }

        @Override
        public void setValue(Integer value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "value=" + value +
                    ", key=" + key;
        }
    }

    private Node<K> head;
    private int size = 0;

    public CountMapImpl() {
        head = new Node<>();
    }

    @Override
    public void add(K k) {
        if (size == 0) {
            head.setKey(k);
            head.setValue(++head.value);
            size++;
        } else {
            Node<K> node = head;
            while (node.next != null) {
                if (node.next.getKey().equals(k)) {
                    node.next.setValue(++node.next.value);
                    return;
                }
                node = node.next;
            }
            if (node.getKey().equals(k)) {
                node.setValue(++node.value);
                return;
            }
            Node<K> newNode = new Node<>();
            newNode.setValue(++newNode.value);
            newNode.setKey(k);
            node.next = newNode;
            size++;
        }
    }

    @Override
    public int getCount(K k) {
        Node<K> node = head;
        while (node != null) {
            if (node.getKey().equals(k)) {
                return node.getValue();
            }
            node = node.next;
        }
        return 0;
    }

    @Override
    public int remove(K k) {
        Node<K> node = head;
        Node<K> next = head.next;
        int value = 0;
        if (size == 0) {
            return 0;
        }
        if (head.getKey().equals(k)) {
            value = head.getValue();
            head = head.next;
            size--;
            return value;
        }
        while (next != null) {
            if (next.getKey().equals(k)) {
                node.next = next.next;
                value = next.getValue();
                size--;
                return value;
            }
            node = next;
            next = next.next;
        }
        return 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void addAll(CountMap<? extends K> source) {
        if (source instanceof CountMapImpl) {
            if (((CountMapImpl<? extends K>) source).size() == 0) {
                return;
            }
            Node<K> node;
            for (Node<K> temp = (Node<K>) ((CountMapImpl<? extends K>) source).head;
                 temp != null; ) {
                node = this.head;
                if (this.size == 0) {
                    this.head.setKey(temp.getKey());
                    this.head.setValue(temp.getValue());
                    this.size++;
                    temp = temp.next;
                    continue;
                }
                if (node.getKey().equals(temp.getKey())) {
                    node.setValue(node.getValue() + temp.getValue());
                    temp = temp.next;
                    continue;
                }
                while (node.next != null) {
                    if (node.next.getKey().equals(temp.getKey())) {
                        node.next.setValue(node.next.getValue() + temp.getValue());
                        break;
                    }
                    node = node.next;
                }
                if (node.next == null) {
                    Node<K> newNode = new Node<>();
                    newNode.setValue(temp.getValue());
                    newNode.setKey(temp.getKey());
                    node.next = newNode;
                    this.size++;
                    temp = temp.next;
                    continue;
                }
                temp = temp.next;
            }
        }
    }

    @Override
    public Map<K, Integer> toMap() {
        Map<K, Integer> map = new TreeMap<>();
        Node<K> node = head;
        while (node != null) {
            map.put(node.getKey(), node.getValue());
            node = node.next;
        }
        return map;
    }

    @Override
    public void toMap(Map<K, Integer> destination) {
        Node<K> node = head;
        while (node != null) {
            destination.put(node.getKey(), node.getValue());
            node = node.next;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Node<K> node = head;
        while (node != null) {
            stringBuilder.append(node.toString());
            stringBuilder.append(" ");
            node = node.next;
        }
        return stringBuilder.toString();
    }
}
