package lesson3.interfaces;

import java.util.Map;

public interface CountMap<K> {
    void add(K k);

    int getCount(K k);

    int remove(K k);

    int size();

    void addAll(CountMap<? extends K> source);

    Map<? extends K, Integer> toMap();

    void toMap(Map<K, Integer> destination);

    interface Entry<K> {
        K getKey();

        Integer getValue();

        void setKey(K k);

        void setValue(Integer value);
    }
}
