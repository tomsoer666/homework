package lesson3;

import lesson3.interfaces.CountMap;
import lesson3.interfaces.impl.CountMapImpl;

import java.util.Map;

public class ApplicationStart {
    public static void main(String[] args) {
        CountMap<String> countMap = new CountMapImpl<>();
        CountMap<String> countMap2 = new CountMapImpl<>();
        countMap.add("Client1");
        countMap.add("Client1");
        countMap.add("Client2");
        countMap.add("Client3");
        countMap2.add("Client4");
        countMap2.add("Client3");
        countMap2.add("Client5");
        countMap2.add("Client6");
        countMap2.add("Client6");
        countMap2.add("Client1");
        System.out.println("Count map " + countMap);
        System.out.println("Count map2 " + countMap2);
        System.out.println("Count map, count Client1 = " + countMap.getCount("Client1"));
        System.out.println("Count map, removed count = " + countMap.remove("Client1"));
        System.out.println("Count map size = " + countMap.size());
        countMap.addAll(countMap2);
        System.out.println("Count map + count map2 " + countMap);
        countMap.remove("Client1");
        countMap.remove("Client3");
        countMap.remove("Client5");
        Map<?, Integer> map = countMap.toMap();
        System.out.println("Count map to map = " + map);
        countMap2.toMap((Map<String, Integer>) map);
        System.out.println("Count map2 add to map = " + map);
    }
}
