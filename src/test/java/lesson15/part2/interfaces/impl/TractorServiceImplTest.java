package lesson15.part2.interfaces.impl;

import junit.framework.TestCase;
import lesson15.part2.enums.Command;
import lesson15.part2.enums.Orientation;
import lesson15.part2.exceptions.TractorInDitchException;
import lesson15.part2.interfaces.TractorService;
import lesson15.part2.models.Field;
import lesson15.part2.models.Tractor;

public class TractorServiceImplTest extends TestCase {

    public void testShouldMoveForward() {
        Tractor tractor = new Tractor();
        Field field = new Field(5, 5);
        TractorService tractorService = new TractorServiceImpl(field, tractor);
        tractorService.move(Command.FORWARD);
        assertEquals(0, tractor.getPositionX());
        assertEquals(1, tractor.getPositionY());
    }

    public void testShouldTurn() {
        Tractor tractor = new Tractor();
        Field field = new Field(5, 5);
        TractorService tractorService = new TractorServiceImpl(field, tractor);
        tractorService.move(Command.TURN_CLOCKWISE);
        assertEquals(Orientation.EAST, tractor.getOrientation());
        tractorService.move(Command.TURN_CLOCKWISE);
        assertEquals(Orientation.SOUTH, tractor.getOrientation());
        tractorService.move(Command.TURN_CLOCKWISE);
        assertEquals(Orientation.WEST, tractor.getOrientation());
        tractorService.move(Command.TURN_CLOCKWISE);
        assertEquals(Orientation.NORTH, tractor.getOrientation());
    }

    public void testShouldTurnAndMoveInTheRightDirection() {
        Tractor tractor = new Tractor();
        Field field = new Field(5, 5);
        TractorService tractorService = new TractorServiceImpl(field, tractor);
        tractorService.move(Command.TURN_CLOCKWISE);
        tractorService.move(Command.FORWARD);
        assertEquals(1, tractor.getPositionX());
        assertEquals(0, tractor.getPositionY());
        tractorService.move(Command.TURN_CLOCKWISE);
        tractorService.move(Command.FORWARD);
        assertEquals(1, tractor.getPositionX());
        assertEquals(-1, tractor.getPositionY());
        tractorService.move(Command.TURN_CLOCKWISE);
        tractorService.move(Command.FORWARD);
        assertEquals(0, tractor.getPositionX());
        assertEquals(-1, tractor.getPositionY());
        tractorService.move(Command.TURN_CLOCKWISE);
        tractorService.move(Command.FORWARD);
        assertEquals(0, tractor.getPositionX());
        assertEquals(0, tractor.getPositionY());
    }

    public void testShouldThrowExceptionIfFallsOffPlateau() {
        Tractor tractor = new Tractor();
        Field field = new Field(5, 5);
        TractorService tractorService = new TractorServiceImpl(field, tractor);
        tractorService.move(Command.FORWARD);
        tractorService.move(Command.FORWARD);
        tractorService.move(Command.FORWARD);
        tractorService.move(Command.FORWARD);
        tractorService.move(Command.FORWARD);
        try {
            tractorService.move(Command.FORWARD);
            fail("Tractor was expected to fall off the plateau");
        } catch (TractorInDitchException expected) {
        }
    }
}