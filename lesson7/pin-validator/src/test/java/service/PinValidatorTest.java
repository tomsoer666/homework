package service;

import exceptions.ClientNotExistsException;
import models.Card;
import org.junit.Assert;
import org.junit.Test;

public class PinValidatorTest {

    @Test(expected = ClientNotExistsException.class)
    public void nullClientTest() {
        new PinValidator().isPinCodeValid(1234, null);
    }

    @Test
    public void correctPinCodeTest() {
        Card client = new Card("Ilya",
                "Trukhov", "Igorevich",
                1000L, 1234);
        Assert.assertTrue(new PinValidator().isPinCodeValid(1234, client));
    }
}
