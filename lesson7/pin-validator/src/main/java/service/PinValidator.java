package service;

import enums.AccountStatus;
import enums.CardStatus;
import exceptions.AccountIsLockedException;
import exceptions.ClientNotExistsException;
import exceptions.IncorrectPinException;
import models.Card;

import java.util.Timer;
import java.util.TimerTask;

public class PinValidator {
    private final Timer timer = new Timer();
    private long startTaskTime;


    public boolean isPinCodeValid(int pinCode, Card card) {
        if (card == null) {
            throw new ClientNotExistsException("Клиента не существует");
        }
        if (card.getAccountStatus() == AccountStatus.LOCKED) {
            throw new AccountIsLockedException("Аккаунт заблокирован. До разблокировки: мс = "
                    , startTaskTime - System.currentTimeMillis());
        }
        if (pinCode != card.getPinCode()) {
            card.incrementTry();
            if (card.getCountTry() == 3) {
                TimerTask timerLock = new TimerLock(card);
                card.setAccountStatus(AccountStatus.LOCKED);
                card.accessTry();
                long delay = 5000L;
                timer.schedule(timerLock, delay);
                startTaskTime = System.currentTimeMillis() + delay;
            }
            throw new IncorrectPinException("Неверный пин код");
        }
        card.accessTry();
        card.setCardStatus(CardStatus.ACCESS);
        return true;
    }
}
