package service;

import exceptions.IllegalCashException;
import models.Card;
import org.junit.Test;

public class TerminalServerTest {
    @Test(expected = IllegalCashException.class)
    public void illegalCashExceptionTest() {
        Card client = new Card("Ilya",
                "Trukhov", "Igorevich",
                1000L, 1234);
        new TerminalServer().sendCash(true, client, 99);
    }
}
