package interfaces.impl;

import exceptions.CardAlreadyInTerminalException;
import exceptions.CardNotInTerminalException;
import exceptions.WrongCardException;
import models.Card;
import service.PinValidator;
import service.TerminalServer;

public class TerminalImpl implements interfaces.Terminal {
    private final TerminalServer server = new TerminalServer();
    private final PinValidator pinValidator = new PinValidator();
    private boolean pinCodeValid = false;

    private static class TerminalCardReader {
        private Card card;
        private boolean cardInTerminal = false;

        public TerminalCardReader() {
        }

        public TerminalCardReader(Card card, boolean cardInTerminal) {
            this.card = card;
            this.cardInTerminal = cardInTerminal;
        }

        public Card getCard() {
            return card;
        }

        public void setCard(Card card) {
            this.card = card;
        }

        public boolean isCardInTerminal() {
            return cardInTerminal;
        }

        public void setCardInTerminal(boolean cardInTerminal) {
            this.cardInTerminal = cardInTerminal;
        }
    }

    private TerminalCardReader terminalCardReader = new TerminalCardReader();

    @Override
    public void insertCard(Card card) {
        if (terminalCardReader.isCardInTerminal()) {
            throw new CardAlreadyInTerminalException("Терминал уже занят картой");
        }
        terminalCardReader = new TerminalCardReader(card, true);
    }

    @Override
    public void enterPinCode(int pinCode, Card card) {
        if (!terminalCardReader.isCardInTerminal()) {
            throw new CardNotInTerminalException("В терминале нет карты");
        }
        if (!terminalCardReader.getCard().equals(card)) {
            throw new WrongCardException("Другая карта в терминале, неверная операция");
        }
        pinCodeValid = pinValidator.isPinCodeValid(pinCode, card);
    }

    @Override
    public void pullOutCard() {
        if (!terminalCardReader.isCardInTerminal()) {
            throw new CardNotInTerminalException("В терминале нет карты");
        }
        terminalCardReader.setCard(null);
        terminalCardReader.setCardInTerminal(false);
    }

    @Override
    public void putCash(long cash, Card card) {
        if (!terminalCardReader.isCardInTerminal()) {
            throw new CardNotInTerminalException("В терминале нет карты");
        }
        if (!terminalCardReader.getCard().equals(card)) {
            throw new WrongCardException("Другая карта в терминале, неверная операция");
        }
        server.sendCash(pinCodeValid, card, cash);
    }

    @Override
    public void withdrawCash(long cash, Card card) {
        if (!terminalCardReader.isCardInTerminal()) {
            throw new CardNotInTerminalException("В терминале нет карты");
        }
        if (!terminalCardReader.getCard().equals(card)) {
            throw new WrongCardException("Другая карта в терминале, неверная операция");
        }
        server.takeCash(pinCodeValid, card, cash);
    }

    @Override
    public String cardInfo(Card card) {
        if (!terminalCardReader.isCardInTerminal()) {
            throw new CardNotInTerminalException("В терминале нет карты");
        }
        if (!terminalCardReader.getCard().equals(card)) {
            throw new WrongCardException("Другая карта в терминале, неверная операция");
        }
        return server.getCardInfo(pinCodeValid, card);
    }

    @Override
    public boolean isCardInTerminal() {
        return terminalCardReader.isCardInTerminal();
    }
}
