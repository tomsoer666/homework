package interfaces;

import exceptions.CardAlreadyInTerminalException;
import exceptions.NoAccessRightException;
import interfaces.impl.TerminalImpl;
import models.Card;
import org.junit.Test;

public class TerminalTest {
    @Test(expected = CardAlreadyInTerminalException.class)
    public void cardInTerminalExceptionTest() {
        Card client = new Card("Ilya",
                "Trukhov", "Igorevich",
                1000L, 1234);
        Terminal terminal = new TerminalImpl();
        terminal.insertCard(client);
        terminal.insertCard(client);
    }

    @Test(expected = NoAccessRightException.class)
    public void wrongOperationExceptionTest() {
        Card client = new Card("Ilya",
                "Trukhov", "Igorevich",
                1000L, 1234);
        Terminal terminal = new TerminalImpl();
        terminal.insertCard(client);
        terminal.putCash(1000L, client);
    }
}
