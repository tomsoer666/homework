package exceptions;

/*
 * Исключение вызывает при неправильном вводе пин кода.
 * Для избежания необходимо ввести правильно пин код.
 * */
public class IncorrectPinException extends RuntimeException {
    public IncorrectPinException(String message) {
        super(message);
    }
}
