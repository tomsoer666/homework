package exceptions;

/*
 * Исключение вызывает при попытке вызвать опертацию в терминале,
 * при отствутствие карты в терминале. Для избежания необходимо
 * проверить наличие карты в терминале.
 * */
public class CardNotInTerminalException extends RuntimeException {
    public CardNotInTerminalException(String message) {
        super(message);
    }
}
