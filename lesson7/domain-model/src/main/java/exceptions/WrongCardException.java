package exceptions;

/*
 * Исключение вызывает при попытке вызвать опертацию в терминале,
 * при не совпадении передаваемой картой на сервер и
 * карты находящийся уже в терминале. Для избежания необходимо
 * передать на сервис ту карту, которая уже находится в терминале.
 * */
public class WrongCardException extends RuntimeException {
    public WrongCardException(String message) {
        super(message);
    }
}
