package service;

import enums.AccountStatus;
import models.Card;

import java.util.TimerTask;

public class TimerLock extends TimerTask {
    private Card card;

    public TimerLock(Card card) {
        this.card = card;
    }

    @Override
    public void run() {
        card.setAccountStatus(AccountStatus.UNLOCKED);
    }
}
