package enums;

public enum AccountStatus {
    UNLOCKED,
    LOCKED
}
